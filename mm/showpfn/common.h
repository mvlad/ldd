#ifndef __COMMON_H
#define __COMMON_H

/* basic lkm */
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/types.h>

#include <linux/sched.h>        /* For current */
#include <linux/list.h>
/* page permissions */
#include <linux/version.h>
#include <asm/cacheflush.h>

#include <linux/syscalls.h>

#include <linux/pid.h>
#include <asm/tlb.h>
#include <asm/tlbflush.h>

#include <linux/mm_types.h>
#include <linux/rbtree.h>
#include <linux/rwsem.h>
#include <linux/spinlock.h>
#include <linux/cpumask.h>

#include <asm/atomic.h>
#include <asm/pgtable.h>

#include <linux/vmalloc.h>
#include <linux/highmem.h>
#include <linux/percpu.h>

/* proc_dir_entry */
#include <linux/proc_fs.h>
#include <linux/seq_file.h>

/* helper functions, vmalloc, kmalloc */
#include <linux/mm.h>
#include <linux/stddef.h>
#include <linux/slab.h>
#include <linux/unistd.h>
#include <linux/string.h>

#include <linux/swap.h>
#include <linux/swapops.h>

#endif /* end __COMMON_H_ */
