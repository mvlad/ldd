#define _GNU_SOURCE
#define _LARGEFILE64_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <byteswap.h>
#include <stdint.h>
#include <sys/types.h>

#define BUF_SIZE	1024
#define PAGE_SIZE	4096
#define PAGE_SHIFT      12
#define PFN_SIZE        4096 * 4096

#define IS_BIT_SET(val, pos)	((val) & (1UL << (pos)))

const char *kpagecount = "/proc/kpagecount";
const char *kpageflags = "/proc/kpageflags";

int main(int argc, char *argv[])
{
	FILE *fp;
	int pid, fdmap;
        int kfdflags, kfdcount;
	char *maps;
	char *pagemap;

	char buf[BUF_SIZE];

	unsigned long addr_start, addr_end;
        int nr_ptes;
        int total_nr_ptes;
	ssize_t nread;

        uint64_t pte, pfn;
	uint64_t *pfns;

	off_t off;
	unsigned long index;

	int j;

	addr_start = addr_end = 0UL;

	if (argc < 2) {
		exit(EXIT_FAILURE);
	}

	pid = atoi(argv[1]);

	asprintf(&maps, "/proc/%d/maps", pid);
	asprintf(&pagemap, "/proc/%d/pagemap", pid);

	fp = NULL;

	if ((fp = fopen(maps, "r")) == NULL) {
		fprintf(stderr, "# could not open %s\n", maps);
		goto out1;
	}

	if ((fdmap = open(pagemap, O_RDONLY)) == -1) {
		fprintf(stderr, "# could not open %s\n", pagemap);
		goto out1;
	}


        if ((kfdflags = open(kpageflags, O_RDONLY)) == -1) {
                fprintf(stderr, "# could not open %s\n", kpageflags);
                goto out1;
        }


        total_nr_ptes = 0;

	pfns = malloc(sizeof(uint64_t) * PFN_SIZE);
	memset(pfns, 0, PFN_SIZE);

        j = 0;
read_maps:
	while (fgets(buf, BUF_SIZE, fp) != NULL) {
		char *str = buf;
                int n;

                n = sscanf(str, "%lx-%lx", &addr_start, &addr_end);
                if (n != 2) {
                        fprintf(stderr, "invalid line\n");
                        continue;
                }


                nr_ptes = (addr_end - addr_start) / PAGE_SIZE;
                total_nr_ptes += nr_ptes;

		if (nr_ptes) {

			fprintf(stderr, "# addr_start: '%lx addr_end: '%lx'\n", addr_start, addr_end);
                        fprintf(stderr, "# ptes found %d\n", nr_ptes);

                        index = (addr_start / PAGE_SIZE) * sizeof(unsigned long);

                        if ((off = lseek64(fdmap, index, SEEK_SET)) == -1) {
                                fprintf(stderr, "# failed to seek to index %lx\n", index);
				/* re-start loop */
				goto read_maps;
                        }
read_pages:
                        while (nr_ptes-- > 0) {

                                nread = 0;
                                nread = read(fdmap, &pte, sizeof(uint64_t));
                                if (nread < 0) {
                                        fprintf(stderr, "# error reading at %lx\n", addr_start);
                                }

				/* page is present */
                                if (IS_BIT_SET(pte, 63)) {
					/* extract pfn from pa */
					pfn = pte & ((1UL << 55) - 1);

					/* save the pages */
					pfns[j++] = pfn;
                                        fprintf(stderr, "\t\t pte: %lx, pfn: %lx\n", pte, pfn);
                                }

                        }
		}
	}


        if ((kfdcount = open(kpagecount, O_RDONLY)) == -1) {
                fprintf(stderr, "# could not open %s\n", kpagecount);
                goto out1;
        }

	fprintf(stderr, "\n\n ==== pfn list ===\n");

        int total_pfns, distinct_pfn;
        total_pfns = distinct_pfn = 0;

        uint64_t *ipfn = pfns;
        while (ipfn && *ipfn) {
		if (*ipfn) {
			/* seek in kernel */
			if ((off = lseek64(kfdcount, *ipfn * 8, SEEK_SET)) == -1) {
				fprintf(stderr, "# could not seek in kpagecount for pfn %lx\n", pfns[j]);
				goto read_pages;	
			}
			
			uint64_t page_count = 0;
			nread = 0;
			nread = read(kfdcount, &page_count, sizeof(uint64_t));

			if (nread > 0) {
				if (page_count > 0) {
					fprintf(stderr, "# pfn: %lx page_count: %lx\n", *ipfn, page_count);

                                        if (page_count == 1)
                                                distinct_pfn++;
				}
			}
			total_pfns++;
                }
                ipfn++;
	}

        fprintf(stderr, "=== stats ===\n");
        fprintf(stderr, "# total ptes: %d\n", total_nr_ptes);
	fprintf(stderr, "# total pfns: %d\n", total_pfns);
        fprintf(stderr, "# distinct pfns: %d\n", distinct_pfn);

	close(kfdcount);

out1:
	if (fp)
		fclose(fp);
	if (fdmap)
		close(fdmap);
        if (kfdflags)
                close(kfdflags);

	if (maps)
		free(maps);
	if (pagemap)
		free(pagemap);
	if (pfns)
		free(pfns);

	return 0;
}
