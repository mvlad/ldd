#include "common.h"
#include "logging.h"
#include "main.h"

#define PROCFS_PID      "scanpfn.pid"
#define PROCFS_VMA      "scanpfn.vma"


pid_t	gpid;

struct vm_data {
	unsigned long vm_start;	
	unsigned long vm_end;
	int start_shown;
	pte_t *ptes;
	unsigned long num_pages;
	unsigned long page_cursor;
};

struct vm {
	struct vm_data *data;
	int vms;
	int vm_cursor;
};

static struct vm *vm = NULL;

static void clear_vm(void)
{
	int i;
	struct vm_data *data;

        if (vm == NULL) {
		LOG("found vm empty. skipping\n");
                return;
	}

	if (vm->data == NULL) {
		LOG("found vm->data empty. skipping\n");
		return;
	}

	for (i = 0, data = vm->data; i < vm->vms; i++, data++) {
		if (data->ptes != NULL) {
			vfree(data->ptes);
		}
	}
	vfree(vm->data);
        vfree(vm);
}

static int alloc_vm_info(struct vm_area_struct *vm_base)
{
	struct vm_area_struct *vma;
	struct vm_data *data;
	unsigned long alloc_size;
	int errcode = 0;

	if (vm) {
		LOG("found vm already present. Clearing up\n");
		clear_vm();
	}

	vm = vmalloc(sizeof(struct vm));
	if (vm == NULL) {
		LOG("failed to init vm\n");
		errcode = -ENOMEM;
		goto err;
	}
	memset(vm, 0, sizeof(struct vm));

	LOG("vm init\n");
	/* get number of vmas */
	for (vma = vm_base; vma != NULL; vma = vma->vm_next) {
		vm->vms++;
	}

	if (!vm->vms) {
		LOG("invalid number of vma found\n");
		errcode = -EINVAL;
		goto err;
	}

	alloc_size = sizeof(struct vm_data) * vm->vms;
	vm->data = vmalloc(alloc_size);
	if (vm->data == NULL) {
		LOG("failed to init vm_data\n");
		errcode = -ENOMEM;
		goto err;
	}
        LOG("vm->data init\n");

	/* for each vma get number of ptes and allocate space */
	for (vma = vm_base, data = vm->data; 
	     vma != NULL; vma = vma->vm_next, data++) {

		/* number of pages */
		data->num_pages = (vma->vm_end - vma->vm_start) >> PAGE_SHIFT;

		alloc_size = sizeof(pte_t) * data->num_pages;
		data->ptes = vmalloc(alloc_size);
		if (data->ptes == NULL) {
			errcode = -ENOMEM; 
			goto err;
		}
		data->page_cursor = 0;
		data->vm_start = vma->vm_start;
		data->vm_end   = vma->vm_end;
		data->start_shown = 0;
	}
        LOG("done alloc\n");

err:
	return errcode;
}

static int walk_pages(struct mm_struct *mm, unsigned long addr, pte_t *pter)
{
        pgd_t *pgd;
        pmd_t *pmd;
        pud_t *pud;
        pte_t *pte;

        unsigned long ipgd = 0UL;
        unsigned long ipud = 0UL;
        unsigned long ipmd = 0UL;
        unsigned long ipte = 0UL;

        pgd = pgd_offset(mm, addr);
        if (pgd_none(*pgd) || unlikely(pgd_bad(*pgd))) {
                goto out;
        }

        pud = pud_offset(pgd, addr);
        if (pud_none(*pud) || unlikely(pud_bad(*pud))) {
                goto out;
        }
        pmd = pmd_offset(pud, addr);

        if (pmd_none(*pmd) || unlikely(pmd_bad(*pmd))) {
                goto out;
        }

        pte = pte_offset_map(pmd, addr);
        if (!pte) {
                goto out;
        }

        *pter = *pte;

        ipgd = pgd_val(*pgd);
        ipud = pud_val(*pud);
        ipmd = pmd_val(*pmd);
        ipte = pte_val(*pte);
#if 0
        LOG("pgd: %lx, pud: %lx, pmd: %lx, pte: %lx\n", ipgd, ipud, ipmd, ipte);
#endif
        pte_unmap(pte);
        

        return 0;
out:
        return -1;

}

static void save_page_info(struct vm_area_struct *vma, struct vm_data *data)
{
	int j;
	pte_t *ptep = data->ptes;
	unsigned long addr = vma->vm_start;

	for (j = 0; j < data->num_pages; 
	     j++, addr += PAGE_SIZE, ptep++) {
		if (walk_pages(vma->vm_mm, addr, ptep) < 0) {
			/* it so happens if high enough pages 
			 * haven't been touched 
			 * pmd doesn't yet exists
			 */
			*ptep = __pte(0);
		}
	}
}

static void store_vm_info(struct vm_area_struct *vm_base)
{
	struct vm_area_struct *vma;
	struct vm_data *data;

        LOG("store_vm_info()\n");

	for (vma = vm_base, data = vm->data; 
	     vma != NULL; vma = vma->vm_next, data++) {
		save_page_info(vma, data);
	}
}

/*
 * main function to set-up pid data
 */
static int setup_pid(pid_t pid)
{
	struct mm_struct *mm = NULL;
	struct pid *tpid;
	struct task_struct *tsk;
	int err = -EINVAL;

	tpid = find_get_pid(pid);
	if (tpid == NULL) {
		LOG("pid %d not found\n", pid);
		goto exit_err;
	}
	tsk = pid_task(tpid, PIDTYPE_PID);
	if (tsk == NULL) {
		LOG("could not find task\n");
		goto exit_err;
	}
	if (tsk == current) {
		goto exit_err;
	}

	/* must call mmput after we've dealt with it */
	mm = get_task_mm(tsk);
	if (mm == NULL) {
		LOG("could not get mm for task\n");
		goto exit_err;
	}

	down_read(&mm->mmap_sem);
	if ((err = alloc_vm_info(mm->mmap)) < 0) {
		LOG("could not alloc\n");
		goto exit_err;
	}

	spin_lock(&mm->page_table_lock);
	store_vm_info(mm->mmap);
	spin_unlock(&mm->page_table_lock);

	err = 0;

exit_err:
	if (mm) {
		up_read(&mm->mmap_sem);	
		mmput(mm);
	}

	return err;
}

static unsigned long int 
user_atoul(const char __user *buf, int len)
{
	char tmp[16];
	unsigned long ret;

        memset(tmp, 0, 16);

	if (len > 15) {
                LOG("user_atoul pid length too big\n");
		return -EINVAL;
	}

	if (copy_from_user(tmp, buf, len)) {
		LOG("user_atoul() failed\n");
		return -EINVAL;
	}
	tmp[len] = 0;

	ret = simple_strtoul(tmp, NULL, 0);
        LOG("user_atoul got pid %ld\n", ret);

	return ret;
}

static int show_one_page(pte_t pte, struct seq_file *seq)
{
	int len, present, writable;
	unsigned long pfn_tmp;
	unsigned long pfn = 0UL;
	swp_entry_t swap_entry;

        len = 0;
	swap_entry.val = 0UL;

	present  = pte_present(pte);
	writable = pte_write(pte) ? 1 : 0;

	if (present) {
		pfn_tmp = pte_pfn(pte);
		if (!pfn_valid(pfn_tmp)) {
			pfn_tmp = 0;
		}
		pfn = pfn_tmp;
	} else {
		swap_entry = pte_to_swp_entry(pte);
		pfn = swap_entry.val;
	}

	len = seq_printf(seq, "%d %d %lx %lx\n", present, writable, pte_val(pte), pfn);

	return len;
}

static int show_vm_start(struct vm_data *data, struct seq_file *seq)
{
	int len = 0;
	len = seq_printf(seq, "VMA %lx:%lx %lu\n", data->vm_start, data->vm_end, data->num_pages);
	return len;
}

/*
 * get pid info
 */

/* seq_printf returns -1 on err, 0 on success */
static ssize_t
scanpfn_pid_write(struct file *file, const char __user *buf, 
		  size_t len, loff_t *ppos)
{
	pid_t pid;
	int errcode = -EINVAL;

	pid = user_atoul(buf, len);
	if (pid <= 0) {
		LOG("invalid pid given\n");
		return errcode;
	}

	if ((errcode = setup_pid(pid)) < 0) {
		LOG("setup_pid() failed\n");
		return errcode;
	}
	gpid = pid;

	return len;
}

/* seq_printf returns -1 on err, 0 on success */
static int scanpfn_pid_show(struct seq_file *seq, void *offset)
{
	int len = 0;
	len = seq_printf(seq, "%d\n", gpid);
	return len;
}

static int scanpfn_vma_show(struct seq_file *seq, void *offset)
{
	int line_len;
	struct vm_data *data;
	pte_t pte;
	int j, i;

	/* sub-sequent times we read it requires it */
	for (i = 0; i < vm->vms; i++) {
		data = vm->data + i;
		if (data->start_shown)
			data->start_shown = 0;
	}

	line_len = 0;
	for (i = 0; i < vm->vms; i++) {
		data = vm->data + i;

		/* show each addr */
		if (!data->start_shown) {
			line_len += show_vm_start(data, seq);

			if (line_len < 0) {
				goto finished;
			}

			data->start_shown = 1;
		}

		/* for each vaddr show pfns */
		for (j = 0; j < data->num_pages; j++) {
			pte = data->ptes[j];
			line_len += show_one_page(pte, seq);

			if (line_len < 0) {
				goto finished;
			}
		}
	}

finished:
	return 0;
}

static int scanpfn_pid_open(struct inode *inode, struct file *file)
{
        return single_open(file, scanpfn_pid_show, NULL);
}

static const struct file_operations scanpfn_pid_ops = {
	.owner          = THIS_MODULE,
        .open           = scanpfn_pid_open,
        .read           = seq_read,
        .llseek         = seq_lseek,
	.write		= scanpfn_pid_write,
        .release        = single_release,
};

static int scanpfn_vma_open(struct inode *inode, struct file *file)
{
	int errcode = -EINVAL;

	if ((errcode = setup_pid(gpid)) < 0) {
		LOG("setup_pid() failed\n");
		return errcode;
	}

        return single_open(file, scanpfn_vma_show, NULL);
}

static const struct file_operations scanpfn_vma_ops = {
	.owner          = THIS_MODULE,
        .open           = scanpfn_vma_open,
        .read           = seq_read,
        .llseek         = seq_lseek,
        .release        = single_release,
};



static int __init init_scan(void)
{
        proc_create(PROCFS_PID, S_IWUSR | S_IRUGO, NULL, &scanpfn_pid_ops);
        proc_create(PROCFS_VMA, S_IWUSR | S_IRUGO, NULL, &scanpfn_vma_ops);

	LOG("module loaded\n");

        return 0;
}

static void __exit exit_scan(void)
{

	remove_proc_entry(PROCFS_PID, NULL);
	remove_proc_entry(PROCFS_VMA, NULL);

        clear_vm();

        LOG("module unloaded\n");
}

module_init(init_scan);
module_exit(exit_scan);

MODULE_AUTHOR("Marius Vlad<marius.vlad0@gmail.com>");
MODULE_DESCRIPTION("Scan and show physical pages of a process");
MODULE_LICENSE("GPL");
