SCANPFN
=======

Display VMA and page frame numbers of a process.


Usage
-----

Module creates two /proc files `scanpfn.pid` and `scanpfa.vma`.

    $ echo PID > /proc/scanpfn.pid
    $ cat /proc/scanpfn.vma

If kernel has been compiled with kpagecount and kpageflags
you can `pagemap/showpfn.c` to display the physical page
frames and their count. Need to be root and provide a PID.

More documentation @ https://www.kernel.org/doc/Documentation/vm/pagemap.txt 
