#ifndef __LOG_H
#define __LOG_H

#define R_LOG(level, format, args...) printk(level "[scanpfn]: %s, %d: "format, __FUNCTION__, __LINE__, ##args);
#define LOG(format, args...) R_LOG(KERN_DEBUG, format, ##args)

#endif /* endif __LOG_H */
