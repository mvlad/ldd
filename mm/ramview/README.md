RAM
===

Simple module and termios app to read/display raw 
memory contents.

Adapted from http://www.cs.usfca.edu/~cruse/cs635f07/dram.c
