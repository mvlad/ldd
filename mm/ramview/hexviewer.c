#define _LARGEFILE64_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <termios.h>

#define MAXNAME	        80

#define BUFHIGH         24
#define BUFWIDE         16
#define BUFSIZE         512

#define ROW	        6
#define COL	        2

#define KB_SEEK         0x0000000A
#define KB_QUIT	        0x0000001B

#define KB_BACK         0x0000007F
#define KB_HOME	        0x00485b1b
#define KB_END          0x00465b1b

#define KB_LNUP         0x00415B1B
#define KB_LNDN         0x00425B1B

#define KB_LEFT         0x00445B1B
#define KB_RGHT         0x00435B1B

#define KB_PGUP	        0x00355B1B
#define KB_PGDN         0x00365B1B

#define KB_DEL          0x00335B1B

char buf[BUFSIZE];
char outline[120];

static void 
tty_init(struct termios *tty_o)
{
        struct termios new_tty;

        tcgetattr(STDIN_FILENO, tty_o);

        new_tty = *tty_o;

        /* 
         * adjust termios to handle 
         * read input without buffering 
         */
        new_tty.c_lflag &= ~(ECHO | ICANON);

        new_tty.c_cc[VMIN]  = 1;
        new_tty.c_cc[VTIME] = 0;

        tcsetattr(STDIN_FILENO, TCSAFLUSH, &new_tty);	
}

static void 
tty_reset(struct termios *tty_o)
{
        tcsetattr(STDIN_FILENO, TCSAFLUSH, tty_o);	
}

static void 
xread(int fd, char *where, size_t to_read)
{
        while (to_read > 0) {
                int nbytes = read(fd, where, to_read);
                if (nbytes <= 0) {
                        break; 
                }
                to_read -= nbytes;
                where += nbytes;
        }
}

static void 
draw_ascii_line(int x, int *len)
{
        int j;

        for (j = 0; j < BUFWIDE; j++) {
                char ch = buf[x * BUFWIDE + j];
                if ((ch < 0x20) || (ch > 0x7E)) {
                        ch = '.';
                }
                *len += sprintf(outline + *len, "%c", ch);
        }
}

static void 
draw_line(int format, long long loc)
{
        int i, j;

        /* display the data just read buf array */
        unsigned char *bp;

        unsigned short *wp;
        unsigned int *dp;
        unsigned long long *qp;

        for (i = 0; i < BUFHIGH; i++) {
                int len;

                /* draw line-location */
                len = sprintf(outline, "%013llX ", loc);

                /* draw the line in the selected hexadecimal format */
                switch (format) {
                        case 1:	/* 'byte' */
                                bp = (unsigned char *) &buf[i * BUFWIDE];
                                for (j = 0; j < BUFWIDE; j++)
                                        len += sprintf(outline + len, 
                                                                "%02X ", 
                                                                bp[j]);
                                break;
                        case 2:	/* 'word' */
                                wp = (unsigned short *) &buf[i * BUFWIDE];
                                for (j = 0; j < BUFWIDE / 2; j++)
                                        len += sprintf(outline + len, 
                                                                " %04X ", 
                                                                wp[j]);
                                break;
                        case 4:	/* 'dword' */
                                dp = (unsigned int *) &buf[i * BUFWIDE];
                                for (j = 0; j < BUFWIDE / 4; j++)
                                        len += sprintf(outline + len, 
                                                                "  %08X  ", 
                                                                dp[j]);
                                break;
                        case 8:	/* 'qword' */
                                qp = (unsigned long long *) &buf[i * BUFWIDE];
                                for (j = 0; j < BUFWIDE / 8; j++)
                                        len += sprintf(outline + len, 
                                                        "    %016llX    ", 
                                                        qp[j]);
                                break;
                        case 16: /* 'octaword' */
                                qp = (unsigned long long *) &buf[i * BUFWIDE];
                                len += sprintf(outline + len, 
                                               "     ");
                                len += sprintf(outline + len, 
                                                "   %016llX%016llX   ", 
                                                qp[1], qp[0]);
                                len += sprintf(outline + len, 
                                                "     "); 
                                break;
                }

                /* draw the line in ascii format */
                draw_ascii_line(i, &len);

                /* transfer this output-line to the screen  */
                printf("\e[%d;%dH%s", ROW + i, COL, outline);

                /* advance loc for the next output-line */
                loc += BUFWIDE;
        }
}

static void handle_seek(long long *pos)
{
        int i = 0;
        char inbuf[16] = {0};

        printf("\e[%d;%dHAddress: ", BUFHIGH + 6, COL);
        fflush(stdout);

        while (i < 15) {
                long long ch = 0;

                read(STDIN_FILENO, &ch, sizeof(ch));
                ch &= 0xffffff;

                if (ch == '\n') {
                        break;
                }
                if (ch == KB_QUIT) { 
                        inbuf[0] = 0; 
                        break; 
                }
                if (ch == KB_LEFT) {
                        ch = KB_BACK;
                }
                if (ch == KB_DEL) {
                        ch = KB_BACK;
                }
                if ((ch == KB_BACK) && (i > 0)) { 
                        inbuf[--i] = 0; 
                        printf("\b \b"); 
                        fflush(stdout);
                }
                if ((ch < 0x20) || (ch > 0x7E)) {
                        continue;
                }

                inbuf[i++] = ch;
                printf("%c", (int) ch);
                fflush(stdout);
        }		
        printf("\e[%d;%dH%70s", BUFHIGH + 6, COL, " ");
        fflush(stdout);

        *pos = strtoull(inbuf, NULL, 16);

        /* paragraph align */
        *pos &= ~0xfLL;
}

int main(int argc, char *argv[])
{
        int fd, k;
        char *fn;

        char info[80];

        int format;
        long long fsize;
        long long pos, loc; 
        long long inc_min, inc_max;
        long long pos_min, pos_max;

        /* save here `normal' termios behaviour */
        struct termios tty_orig;

        if (argc < 2) {
                fprintf(stderr, "%s filename\n", argv[0]);
                exit(EXIT_FAILURE); 
        }

        fn = argv[1];
        if ((fd = open(fn, O_RDONLY)) == -1) {
                fprintf(stderr, "could not open '%s': '%s'\n", 
                        fn, strerror(errno)); 
                exit(EXIT_FAILURE); 
        }

        if ((fsize = lseek64(fd, 0LL, SEEK_END)) < 0LL) {
                fprintf(stderr, "cannot locate EOF\n"); 
                exit(EXIT_FAILURE); 
        }

        inc_min = (1LL <<  8);
        inc_max = (1LL << 36);		

        pos_min = 0LL;
        pos_max = (fsize - 241LL) & ~0xf;

        if (pos_max < pos_min) {
                pos_max = pos_min;
        }

        /* initiate noncanonical terminal input */
        tty_init(&tty_orig);
        printf("\e[H\e[J");

        /* draw filename */
        k = (77 - strlen(fn)) / 2;
        printf("\e[%d;%dH\'%s\'", 3, k, fn);

        sprintf(info, "filesize: %llu (=0x%013llX)", 
                        fsize, fsize);

        /* draw filesize */
        k = (78 - strlen(info));
        printf("\e[%d;%dH%s", BUFHIGH + 8, k, info);

        fflush(stdout);


        long long page_incr = inc_min;
        pos = loc = 0LL;
        format = 1;

        for (;;) {
                /* erase prior buffer contents */
                memset(buf, ~0, BUFSIZE);

                /* restore 'pageincr' to prescribed bounds */
                if (page_incr == 0LL) {
                        page_incr = inc_max;
                } else if (page_incr < inc_min) {
                        page_incr = inc_min;
                } else if (page_incr > inc_max) {
                        page_incr = inc_max;
                }

                /* get current location */
                loc = lseek64(fd, pos, SEEK_SET);

                /* fill buf with data from the file */
                char *where = buf;
                int to_read = BUFSIZE;
                xread(fd, where, to_read);

                draw_line(format, loc);

                printf("\e[%d;%dH", BUFHIGH + 6, COL);
                fflush(stdout);	

                /* 
                 * getchar() only stores a int and
                 * PG_UP, PG_DOWN requires 3 int
                 *
                 * use standard read to get it properly
                 *
                 */
                long long ch = 0LL;
                read(STDIN_FILENO, &ch, sizeof(ch));

                /* draw position */
                printf("\e[%d;%dH%60s", BUFHIGH + 6, COL, " ");	

                /* interpret navigation or formatting command */
                switch (ch & 0x00ffffffLL) {
                        /* move to the file's beginning/ending */
                        case KB_HOME:
                                pos = pos_min;
                                break;
                        case KB_END:
                                pos = pos_max;
                                break;
                        /* move forward/backward by one line */
                        case KB_LNDN:
                                pos += BUFWIDE;
                                break;
                        case KB_LNUP:
                                pos -= BUFWIDE;
                                break;
                        /* move forward/packward by one page */
                        case KB_PGDN:
                                pos += page_incr;
                                break;
                        case KB_PGUP:
                                pos -= page_incr;
                                break;
                        /* increase/decrease the page-size increment */
                        case KB_RGHT:
                                page_incr >>= 4;
                                break;
                        /* reset the hexadecimal output-format */
                        case KB_LEFT:
                                page_incr <<= 4;
                                break;
                        case 'w':
                                format = 1;
                                break;
                        case 'e':
                                format = 2;
                                break;
                        case 'r':
                                format = 4;
                                break;
                        case 't':
                                format = 8;
                                break;
                        case 'y':
                                format = 16;
                                break;
                        /* seek to a user-specified file-position */
                        case 'F':
                                handle_seek(&pos);
                                break;
                        case 'q':
                                goto out;
                        default:
                                printf("\e[%d;%dHPressed key '%llx'", 
                                                BUFHIGH + 6, 2, ch);
                                break;
                }
                fflush(stdout);

                /* pos within bounds */
                if (pos < pos_min) {
                        pos = pos_min;
                } else if (pos > pos_max) {
                        pos = pos_max;
                }
        }

out:
        fflush(stdout);

        /* restore canonical terminal behavior */
        tty_reset(&tty_orig);

        printf("\e[%d;%dH\e[0J\n", BUFHIGH + 8, 0);

        return 0;
}
