#include <linux/module.h>
#include <linux/mm.h>
#include <linux/fs.h>
#include <asm/io.h>
#include <asm/uaccess.h>
#include <linux/version.h>
#include <linux/cdev.h>
#include <linux/device.h>

#define R_LOG(level, format, args...) do {                                      \
        printk(level "[ram]: %s, %d: "format, __FUNCTION__, __LINE__, ##args);  \
} while (0)

#define LOG(format, args...) \
        R_LOG(KERN_DEBUG, format, ##args)

unsigned long max_ram;

ssize_t ram_read(struct file *file, char *buf, 
                size_t count, loff_t *pos)
{
        void *va;

        if (*pos >= max_ram) {
                return 0;
        }

        /* we can only read up to the end of physical memory */
        if (*pos + count > max_ram) {
                count = max_ram - *pos;
        }

        /* physical address to mapped virtual address */
        va = phys_to_virt(*pos);

        /* send to user space */
        if (copy_to_user(buf, va, count)) {
                return -EFAULT;
        }

        /* advance file-pointer */
        *pos += count;
        return count;
}


loff_t ram_llseek(struct file *file, loff_t offset, int whence)
{
        loff_t newpos = -1;

        switch (whence) {
                case 0: /* SEEK_SET */
                        newpos = offset; 
                        break;
                case 1: /* SEEK_CUR */
                        newpos = file->f_pos + offset; 
                        break;
                case 2: /* SEEK_END */
                        newpos = max_ram + offset; 
                        break;
        }

        if ((newpos < 0) || (newpos > max_ram)) {
                return -EINVAL;
        }

        file->f_pos = newpos;
        return newpos;
}

struct file_operations read_ops = {
        owner:		THIS_MODULE,
        llseek:		ram_llseek,
        read:		ram_read,
};

static dev_t ram_dev;
static struct cdev ram_cdev;
static struct class *ram_class;

#define MOD_NAME        "ram"

#define MOD_MINOR       0
#define MOD_CNT         1

static int __init init_dram(void)
{
        struct device *ram_device;
        if (alloc_chrdev_region(&ram_dev, MOD_MINOR, MOD_CNT, MOD_NAME) < 0) {
                return -1;
        }

        cdev_init(&ram_cdev, &read_ops);
        if (cdev_add(&ram_cdev, ram_dev, MOD_CNT) < 0) {
                unregister_chrdev_region(ram_dev, MOD_CNT);
                return -1;
        }

        ram_class = class_create(THIS_MODULE, "char");
        ram_device = device_create(ram_class, NULL, ram_dev, 
                                   NULL, MOD_NAME);

        if (IS_ERR(ram_device)) {
                class_destroy(ram_class);
                cdev_del(&ram_cdev);
                unregister_chrdev_region(ram_dev, MOD_CNT);
                return PTR_ERR(ram_device);
        }

#if LINUX_VERSION_CODE >= KERNEL_VERSION(3, 11, 0)
        max_ram = get_num_physpages() * PAGE_SIZE;
#else
	max_ram = num_physpages * PAGE_SIZE;
#endif

	LOG("'%s' loaded\n", MOD_NAME);
	LOG("RAM=%016lX (%lu MB)\n", max_ram, max_ram >> 20);

        return 0;
}


static void __exit exit_dram(void)
{
        device_destroy(ram_class, ram_dev);
        class_destroy(ram_class);

        cdev_del(&ram_cdev);
        unregister_chrdev_region(ram_dev, MOD_CNT);

	LOG("'%s' unloaded\n", MOD_NAME);
}

module_init(init_dram);
module_exit(exit_dram);

MODULE_AUTHOR("Marius Vlad<marius.vlad0@gmail.com>");
MODULE_DESCRIPTION("Expose contents of physical RAM");
MODULE_LICENSE("GPL");
