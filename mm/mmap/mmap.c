#include <linux/slab.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/vmalloc.h>
#include <linux/version.h>
#include <linux/fs.h>
#include <linux/mm.h>
#include <linux/stddef.h>
#include <linux/unistd.h>
#include <linux/string.h>

#define NPAGES          16
#define MOD_MINOR       0
#define MOD_CNT         2

static dev_t mmap_dev;
static struct cdev mmap_cdev;
static struct class *cl;

/* pointer to page aligned area */
static int *vmalloc_area = NULL;
static int *kmalloc_area = NULL;

/* pointer to unaligend area */
static int *kmalloc_ptr = NULL;

/* no ops */
static int f_open(struct inode *inode, struct file *file)
{
	return 0;
}

static int f_close(struct inode *inode, struct file *file)
{
	return 0;
}

static int mmap_kmem(struct file *file, struct vm_area_struct *vma)
{
        int ret;
        unsigned long len, addr, pfn;
        pgprot_t prot;

        len = vma->vm_end - vma->vm_start;

        if (len > NPAGES * PAGE_SIZE) {
                return -EIO;
        }

        addr    = vma->vm_start;
        pfn     = virt_to_phys((void *) kmalloc_area) >> PAGE_SHIFT;
        prot    = vma->vm_page_prot;

        if ((ret = remap_pfn_range(vma, addr, pfn, len, prot)) < 0) {
                return ret;
        }

        return 0;
}

static int mmap_vmem(struct file *file, struct vm_area_struct *vma)
{
        int ret;
        unsigned long addr, pfn, len;
        char *ptr;

        len     = vma->vm_end - vma->vm_start;

        if (len > NPAGES * PAGE_SIZE) {
                return -EIO;
        }

	printk(KERN_DEBUG "got len %ld to map\n",
			len);

        addr    = vma->vm_start;
        ptr     = (char *) vmalloc_area;

	printk(KERN_DEBUG "starting mapping at addr %lx pointing at %lx\n",
				addr, *(unsigned long *) &ptr);

        /* 
	 *  loop over all pages, 
	 *  map it page individually 
	 */
        while (len > 0) {

                pfn = vmalloc_to_pfn(ptr);
		printk(KERN_DEBUG "mapped addr %lx pfn %lx\n", 
                                addr, *(unsigned long *) &pfn);

                ret = remap_pfn_range(vma, addr, pfn, 
                                        PAGE_SIZE, PAGE_SHARED);
                if (ret < 0) {
                        return ret;
                }

                addr    += PAGE_SIZE;
                ptr     += PAGE_SIZE;
                len     -= PAGE_SIZE;
        }

        return 0;
}

static int mmap_mmap(struct file *file, struct vm_area_struct *vma)
{
        if (vma->vm_pgoff == 0) {
                return mmap_vmem(file, vma);
        }

        if (vma->vm_pgoff == NPAGES) {
                return mmap_kmem(file, vma);
        }

	return -EIO;
}

/* mmap operations */
static const struct file_operations fops = {
	.owner         = THIS_MODULE,
	.open          = f_open,
	.release       = f_close,
	.mmap          = mmap_mmap,
};

static int __init mmapdrv_init(void)
{
	int i;
	struct device *dev_ret;


	printk(KERN_DEBUG "mmap module init\n");

        if (alloc_chrdev_region(&mmap_dev, MOD_MINOR, MOD_CNT, "mmap") < 0) {
                return -1;
        }

        cdev_init(&mmap_cdev, &fops);

        if (cdev_add(&mmap_cdev, mmap_dev, MOD_CNT) < 0) {
                unregister_chrdev_region(mmap_dev, MOD_CNT);
                return -1;
        }

        cl = class_create(THIS_MODULE, "char");

        dev_ret = device_create(cl, NULL, mmap_dev, NULL, "mmap");

	if (IS_ERR(dev_ret)) {
                class_destroy(cl);
                cdev_del(&mmap_cdev);
                unregister_chrdev_region(mmap_dev, MOD_CNT);
		return PTR_ERR(dev_ret);
	}


	/* 
	 * Get a memory area with kmalloc and 
         * aligned it to a page. 
         * This area will be physically contigous 
         */
	kmalloc_ptr  = kmalloc((NPAGES + 2) * PAGE_SIZE, GFP_KERNEL);
        if (kmalloc_ptr == NULL) {
                printk(KERN_DEBUG "kmalloc() failed\n");
                return -ENOMEM;
        }
	kmalloc_area = (int *)(((unsigned long) kmalloc_ptr + PAGE_SIZE - 1) & PAGE_MASK);


	/* get a memory area that is only virtual contigous. */
	vmalloc_area  = (int *) vmalloc(NPAGES * PAGE_SIZE);
        if (vmalloc_area == NULL) {
                printk(KERN_DEBUG "vmalloc() failed\n");
                return -ENOMEM;
        }


        /* mark the pages reserved */
        for (i = 0; i < NPAGES * PAGE_SIZE; i += PAGE_SIZE) {

                SetPageReserved(vmalloc_to_page((void *) (((unsigned long) vmalloc_area) + i)));

                SetPageReserved(virt_to_page(((unsigned long) kmalloc_area) + i));
        }

        /* store pattern in memory */
	for (i = 0; i < (NPAGES * PAGE_SIZE / sizeof(int)); i += 2) {

		/* 
		 * initialise with some dummy values 
		 * to compare later, from us 
		 */
		vmalloc_area[i]		= (0xaffe << 16) + i;
		vmalloc_area[i + 1]	= (0xbeef << 16) + i;
		kmalloc_area[i]		= (0xdead << 16) + i;
		kmalloc_area[i + 1]	= (0xbeef << 16) + i;
	}

	printk(KERN_DEBUG "vmalloc_area at 0x%p (phys 0x%lx)\n", 
		vmalloc_area, (unsigned long ) __pa(vmalloc_area));

	printk(KERN_DEBUG "kmalloc_area at 0x%p (phys 0x%lx)\n", 
			kmalloc_area, 
			(unsigned long ) virt_to_phys((void *) kmalloc_area));
	return 0;
}

/* remove the module */
static void __exit mmapdrv_exit(void)
{
        int i;

	/* unreserve all pages */
        for (i = 0; i < NPAGES * PAGE_SIZE; i+= PAGE_SIZE) {
                ClearPageReserved(vmalloc_to_page((void *)(((unsigned long) vmalloc_area) + i)));
                ClearPageReserved(virt_to_page(((unsigned long) kmalloc_area) + i));
        }
	/* and free the two areas */
	if (vmalloc_area) {
		vfree(vmalloc_area);
	}

	if (kmalloc_ptr) {
		kfree(kmalloc_ptr);
	}

        device_destroy(cl, mmap_dev);
	class_destroy(cl);

        cdev_del(&mmap_cdev);
        unregister_chrdev_region(mmap_dev, MOD_CNT);
	printk(KERN_DEBUG "mmap module exit\n");
}

module_init(mmapdrv_init);
module_exit(mmapdrv_exit);

MODULE_AUTHOR("Marius Vlad<marius.vlad0@gmail.com>");
MODULE_DESCRIPTION("MMAP, vmalloc and kmalloc");
MODULE_LICENSE("GPL");
