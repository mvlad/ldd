#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define NPAGES 16

int main(void)
{
	int fd;
        int len = NPAGES * getpagesize();

	unsigned int *vadr;
	unsigned int *kadr;

	if ((fd = open("/dev/mmap", O_RDWR)) < 0) {
		perror("open");
		exit(EXIT_FAILURE);
	}

	vadr = mmap(NULL, len, PROT_READ | PROT_WRITE, 
			MAP_SHARED, fd, 0);

	if (vadr == MAP_FAILED) {
		perror("mmap");
		exit(-1);
	}

	fprintf(stderr, "vmalloc()\n");
	if ((vadr[0] != 0xaffe0000) || (vadr[1] != 0xbeef0000) || 
	    (vadr[len / sizeof(int) -2] != (0xaffe0000 + len / sizeof(int) - 2)) || 
	    (vadr[len / sizeof(int) -1] != (0xbeef0000 + len / sizeof(int) - 2))) {

		fprintf(stderr, "0x%x 0x%x\n", 
			vadr[0], vadr[1]);

		fprintf(stderr, "0x%x 0x%x\n", 
			vadr[len / sizeof(int) - 2], 
			vadr[len / sizeof(int) - 1]);
	}
        fprintf(stderr, "0x%x 0x%x\n", vadr[0], vadr[1]);


	kadr = mmap(0, len, PROT_READ | PROT_WRITE, 
			MAP_SHARED | MAP_LOCKED, fd, len);

	if (kadr == MAP_FAILED) {
		perror("mmap");
		exit(-1);
	}

	fprintf(stderr, "kzalloc()\n");
	if ((kadr[0] != 0xdead0000) || (kadr[1] != 0xbeef0000) || 
	    (kadr[len / sizeof(int) - 2] != (0xdead0000 + len / sizeof(int) -2)) || 
	    (kadr[len / sizeof(int) - 1] != (0xbeef0000 + len / sizeof(int) -2))) {
		
		fprintf(stderr, "0x%x 0x%x\n", 
				kadr[0], kadr[1]);

		fprintf(stderr, "0x%x 0x%x\n", 
				kadr[len / sizeof(int) - 2], 
				kadr[len / sizeof(int) - 1]);
	}
        fprintf(stderr, "0x%x 0x%x\n", 
		kadr[0], kadr[1]);

	close(fd);

	return 0;
}
