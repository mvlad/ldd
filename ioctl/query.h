#ifndef __QUERY_H
#define __QUERY_H

#ifdef __KERNEL

typedef struct {
        int ego;
        unsigned int nr_entries;
        struct list_head list;
        spinlock_t lock;
} query_arg_t;

typedef struct {
        unsigned int nr_entries;
        int __user *entries;
} entry_t;

#else

typedef struct {
        unsigned int nr_entries;
        int *entries;
} entry_t;

typedef enum { e_get, e_get_all, e_clr, e_set } option_t;

#endif

/* this just gets one to the list,
 * simple echo mechanism */
#define QUERY_GET_A_VARIABLE    _IOWR('q', 0xa2, int *)

/* get the number of vars */
#define QUERY_GET_NR_VARIABILES         _IOR('q', 0x10, int *)

/* get all vars */
#define QUERY_GET_ALL_VARIABLES         _IOWR('q', 0xa5, entry_t *)

#define QUERY_CLR_VARIABLES     _IO('q', 0xa6)

#define QUERY_SET_VARIABLE     _IOW('q', 0xa8, int *)

#endif /* __QUERY_H */
