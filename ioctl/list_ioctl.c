#define __KERNEL
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/version.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/errno.h>
#include <asm/uaccess.h>
#include <linux/ioctl.h>
#include <linux/slab.h>
#include <linux/spinlock.h>
#include "query.h"

static DEFINE_MUTEX(entries_mutex_lock);
LIST_HEAD(entries);

#define FIRST_MINOR 0
#define MINOR_CNT 1

static dev_t dev;
static struct cdev c_dev;
static struct class *cl;

static int add_entry(int ego, struct list_head *head);
static void empty_entries(struct list_head *head);
static query_arg_t *get_entry(int ego, struct list_head *head);
static unsigned int count_entries(struct list_head *head);
static void list_all_entries(struct list_head *head);

static int my_open(struct inode *i, struct file *f)
{
        return 0;
}
static int my_close(struct inode *i, struct file *f)
{
        return 0;
}

static long my_ioctl(struct file *f, unsigned int cmd, unsigned long arg)
{
        switch (cmd) {
                case QUERY_GET_NR_VARIABILES: { /* get total count */

                        int nr_entries = count_entries(&entries);

                        if (copy_to_user((int *) arg, &nr_entries, sizeof(int *)))
                                return -EFAULT;
                        break;
                }
                case QUERY_GET_ALL_VARIABLES: { /* get all ego vars */

                        query_arg_t *pos;
                        entry_t local_entries;
                        int __user *e;
                         
                        /* where to dump the data */
                        if (copy_from_user(&local_entries, (entry_t *) arg, sizeof(entry_t))) {
                                printk(KERN_DEBUG "failed to get data from user-space\n");
                                return -EFAULT;
                        }

                        if (local_entries.entries == NULL) {
                                printk(KERN_DEBUG "user-space did not set up a valid mem region\n");
                                return -EFAULT;
                        }
                        /* getting from user-space a malloc area... */
                        e = local_entries.entries;
                        list_for_each_entry(pos, &entries, list) {
                                *e = pos->ego;
                                if (copy_to_user((int *) arg, &e, sizeof(int *)))
                                        return -EFAULT;
                                e++;
                        }

                        /* now copy it back, populated */
                        if (copy_to_user((entry_t *) arg, &local_entries, sizeof(entry_t)))
                                return -EFAULT;
                        break;
                }
                case QUERY_GET_A_VARIABLE: { /* simple echo for a ego var */

                        int p;
                        query_arg_t *entry = NULL;

                        printk(KERN_DEBUG "querying for a single var\n");

                        if (copy_from_user(&p, (int *) arg, sizeof(int *))) {
                                printk(KERN_DEBUG "failed to copy from user entry\n");
                                return -EACCES;
                        }
                        printk(KERN_DEBUG "got from user-space to look for %d\n", p);

                        if ((entry = get_entry(p, &entries)) == NULL) {
                                printk(KERN_DEBUG "could not find a valid entry\n");
                                return -ENOENT;
                        }

                        if (copy_to_user((int *) arg, &entry->ego, sizeof(int *)))
                                return -EFAULT;

                        break;
                }
                case QUERY_CLR_VARIABLES: { /* clear all vars */
                        printk(KERN_DEBUG "clearing all variabiles\n");
                        empty_entries(&entries);
                        break;
                }
                case QUERY_SET_VARIABLE: { /* just set one var */
                        int p;
                        printk(KERN_DEBUG "setting a variable\n");
                        if (copy_from_user(&p, (int *) arg, sizeof(int *))) {
                                printk(KERN_DEBUG "failed to copy from user entry \n");
                                return -EACCES;
                        }
                        printk(KERN_DEBUG "got from user-space var %d\n", p);
                        add_entry(p, &entries);
                        printk(KERN_DEBUG "total entries %d\n", count_entries(&entries));
                        list_all_entries(&entries);
                        break;
                }
                default:
                        return -EINVAL;
        }

        return 0;
}

static struct file_operations query_fops =
{
        .owner = THIS_MODULE,
        .open = my_open,
        .release = my_close,
        .unlocked_ioctl = my_ioctl
};

static query_arg_t *init_query(void)
{
        query_arg_t *q;

        q = kzalloc(sizeof(*q), GFP_KERNEL);
        if (!q)
                return NULL;

        INIT_LIST_HEAD(&q->list);
        spin_lock_init(&q->lock);
        q->nr_entries = 0;

        return q;
}

static int add_entry(int ego, struct list_head *head)
{
        query_arg_t *entry = init_query();

        if (!entry)
                return -1;

        spin_lock(&entry->lock);
        entry->ego = ego;
        entry->nr_entries = count_entries(head) + 1;
        list_add_tail(&entry->list, head);
        spin_unlock(&entry->lock);

        return 0;
}

static void empty_entries(struct list_head *head)
{
        query_arg_t *pos;

        mutex_lock(&entries_mutex_lock);
        list_for_each_entry(pos, head, list) {
                pos->ego = 0;
        }
        mutex_unlock(&entries_mutex_lock);
}

static query_arg_t *get_entry(int ego, struct list_head *head)
{
        query_arg_t *pos;

        mutex_lock(&entries_mutex_lock);
        list_for_each_entry(pos, head, list) {
                if (pos->ego == ego) {
                        printk(KERN_DEBUG "found ego var %d\n", ego);
                        mutex_unlock(&entries_mutex_lock);
                        return pos;
                }
        }
        printk(KERN_DEBUG "no entry with %d found\n", ego);
        mutex_unlock(&entries_mutex_lock);

        return NULL;
}
static unsigned int count_entries(struct list_head *head)
{
        query_arg_t *pos;
        unsigned int cnt = 0;

        mutex_lock(&entries_mutex_lock);
        list_for_each_entry(pos, head, list) {
                cnt++;
        }
        mutex_unlock(&entries_mutex_lock);

        return cnt;
}

static void list_all_entries(struct list_head *head)
{
        query_arg_t *pos;

        mutex_lock(&entries_mutex_lock);
        list_for_each_entry(pos, head, list) {
                printk(KERN_DEBUG "entry: %d\n", pos->ego);
        }
        mutex_unlock(&entries_mutex_lock);
}

static int __init query_ioctl_init(void)
{
        int ret;
        struct device *dev_ret;

        ret = alloc_chrdev_region(&dev, FIRST_MINOR, MINOR_CNT, "query_ioctl");
        if (ret < 0) {
                return ret;
        }

        cdev_init(&c_dev, &query_fops);

        ret = cdev_add(&c_dev, dev, MINOR_CNT);
        if (ret < 0) {
                return ret;
        }

        cl = class_create(THIS_MODULE, "char");
        if (IS_ERR(cl)) {
                cdev_del(&c_dev);
                unregister_chrdev_region(dev, MINOR_CNT);
                return PTR_ERR(cl);
        }

        dev_ret = device_create(cl, NULL, dev, NULL, "query");
        if (IS_ERR(dev_ret)) {
                class_destroy(cl);
                cdev_del(&c_dev);
                unregister_chrdev_region(dev, MINOR_CNT);
                return PTR_ERR(dev_ret);
        }

        return 0;
}

static void __exit query_ioctl_exit(void)
{
        device_destroy(cl, dev);
        class_destroy(cl);
        cdev_del(&c_dev);
        unregister_chrdev_region(dev, MINOR_CNT);
}

module_init(query_ioctl_init);
module_exit(query_ioctl_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("tauthor");
MODULE_DESCRIPTION("ioctl");
