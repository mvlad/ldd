#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>
#include <errno.h>
#include "query.h"

#define MAX_ENTRIES 1024

static void get_var(int var, int fd)
{
        int tmp = var;
        fprintf(stderr, ">> trying to get %d\n", var);
        if (ioctl(fd, QUERY_GET_A_VARIABLE, &var) == -1) {
                fprintf(stderr, "failed query_apps "
                                "ioctl get %s\n", strerror(errno));
                exit(EXIT_FAILURE);
        }
        if (tmp == var)
                fprintf(stderr, "# got valid entry\n");
        else
                fprintf(stderr, "# entry not found\n");
}

static void get_vars(int fd)
{
        entry_t q;
        int nr_entries;
        int *ptr;
        
        if (ioctl(fd, QUERY_GET_NR_VARIABILES, &nr_entries) == -1)
                exit(EXIT_FAILURE);

        if (nr_entries == 0) {
                fprintf(stderr, "# received 0 vars\n");
                return;
        }

        q.entries = malloc(sizeof(int) * nr_entries);
        if (q.entries == NULL) {
                fprintf(stderr, "# failed to allocate\n");
                exit(EXIT_FAILURE);
        }

        if (ioctl(fd, QUERY_GET_ALL_VARIABLES, &q) == -1) {
                fprintf(stderr, "query_apps ioctl get\n");
                free(q.entries);
                exit(EXIT_FAILURE);
        }

        /* should iterate here over them */
        fprintf(stderr, "# got %d entries\n", q.nr_entries);
        ptr = q.entries;

        while (ptr && nr_entries--) {
                fprintf(stderr, "# entry %d\n", *ptr);
                ptr++;
        }

        free(q.entries);
}

static void clr_vars(int fd)
{
        if (ioctl(fd, QUERY_CLR_VARIABLES) == -1) {
                fprintf(stderr, "query_apps ioctl clr\n");
                exit(EXIT_FAILURE);
        }
}

void set_var(int var, int fd)
{
        if (ioctl(fd, QUERY_SET_VARIABLE, &var) == -1) {
                fprintf(stderr, "query_apps ioctl set: %s\n", 
                        strerror(errno));
                exit(EXIT_FAILURE);
        }
}

int main(int argc, char *argv[])
{
        char *file_name = "/dev/query";
        option_t option;
        int fd;
        int c;
        int var;

        while ((c = getopt(argc, argv, "g:acs:")) != -1) {
                switch (c) {
                        case 'g':
                                option = e_get;
                                var = atoi(optarg);
                                break;
                        case 'a':
                                option = e_get_all;
                                break;
                        case 'c':
                                option = e_clr;
                                break;
                        case 's':
                                option = e_set;
                                var = atoi(optarg);
                                break;
                        default:
                                fprintf(stderr, "Usage: %s [-g | -c | -s]\n", argv[0]);
                                exit(EXIT_FAILURE);
                }
        }
        if (optind > argc) {
                fprintf(stderr, "Usage: %s [-g | -c | -s]\n", argv[0]);
                exit(EXIT_FAILURE);
        }

        fd = open(file_name, O_RDWR);
        if (fd == -1) {
                fprintf(stderr, "query_apps open '%s'\n", strerror(errno));
                exit(EXIT_FAILURE);
        }

        switch (option) {
                case e_get:
                        fprintf(stderr, "# getting a single var %d\n", var);
                        get_var(var, fd);
                        break;
                case e_get_all:
                        fprintf(stderr, "# getting all vars\n");
                        get_vars(fd);
                        break;
                case e_clr:
                        fprintf(stderr, "# clearing vars\n");
                        clr_vars(fd);
                        break;
                case e_set:
                        fprintf(stderr, "# setting var\n");
                        set_var(var, fd);
                        break;
                default:
                        fprintf(stderr, "# invalid ...\n");
                        break;
        }

        close (fd);

        return 0;
}
