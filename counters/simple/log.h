#ifndef __LOG_H
#define __LOG_H

#include <linux/ratelimit.h>

#define LOG(fmt, args...) printk(KERN_DEBUG "%s:%d " fmt, __FUNCTION__, __LINE__, ##args)
#define LOGL(fmt, args...) printk_ratelimited(KERN_DEBUG "%s:%d " fmt, __FUNCTION__, __LINE__, ##args)

#endif /* __LOG_H */
