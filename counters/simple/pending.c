#include <linux/fs.h>
#include <linux/mm.h>
#include <linux/poll.h>
#include <linux/file.h>
#include <linux/irq_work.h>
#include <linux/signal.h>
#include <linux/sched.h>

#include "main.h"
#include "log.h"

/*
 * pending events 
 */

void wake_up_event(struct nmi_data *data)
{
        LOG("should drain data here\n");
}

void pending_wakeup(struct nmi_data *data)
{
        wake_up_event(data);

        if (data->pending_kill) {
                kill_fasync(&data->fasync, SIGIO, data->pending_kill);
                data->pending_kill = 0;
        }
}

void pending_data(struct irq_work *work)
{
        struct nmi_data *data = container_of(work, struct nmi_data, pending);

        if (data->pending_disable) {
                data->pending_disable = 0;
                /* should stop recording here */
                LOG("should stop recording here\n");
        }

        if (data->pending_wakeup) {
                data->pending_wakeup = 0;
                pending_wakeup(data);
        }
}

static int __overflow_event(struct nmi_data *data, struct pt_regs *regs)
{
        int ret = 0;
        int data_events = atomic_read(&data->nmi_limit);

        data->pending_kill = POLL_IN;
        if (data_events && atomic_dec_and_test(&data->nmi_limit)) {
                ret = 1;
                data->pending_kill = POLL_HUP;
                data->pending_disable = 1;
                irq_work_queue(&data->pending);
        }

        if (data->overflow_handler) {
                /* here we should have a overflow callback */
                data->overflow_handler(data);
        } else {
                /* default action */
                LOG("no overflow handler installed\n");
                LOG("default action should be taken!\n");
        }

        if (data->fasync && data->pending_kill) {
                data->pending_wakeup = 1;
                irq_work_queue(&data->pending);
        }

        return ret;
}

int overflow_event(struct nmi_data *data, struct pt_regs *regs)
{
        LOG("overflow event..\n");
        return __overflow_event(data, regs);
}

void sample_data(struct nmi_data *data, struct buffer *buff)
{
	LOG("should sample data...\n");
}

int nmi_handle_irq(struct pt_regs *regs)
{
	int handled = 0;

	struct nmi_data data;
	struct buffer buff;

	LOG("in handle irq\n");

	/*
         * Some chipsets need to unmask the LVTPC in a particular spot
         * inside the nmi handler.  As a result, the unmasking was pushed
         * into all the nmi handlers.
         *
         * This generic handler doesn't seem to have any issues where the
         * unmasking occurs so it was left at the top.
         */

	apic_write(APIC_LVTPC, APIC_DM_NMI);

	handled++;

	sample_data(&data, &buff);

	if (overflow_event(&data, regs)) {
		/* should stop here ?*/
		LOG("overflowing event returned true\n");
	}
	return handled;
}

int event_nmi_handler(unsigned int cmd, struct pt_regs *regs)
{
	int ret;
	u64 s_clock, e_clock;

	s_clock = local_clock();
	ret = nmi_handle_irq(regs);
	e_clock = local_clock();

	return ret;
}
