#include <linux/kernel.h>
#include <linux/string.h>
#include <linux/slab.h>
#include <asm/ptrace.h>
#include <asm/msr.h>
#include <asm/apic.h>
#include <asm/nmi.h>
#include <linux/percpu.h>
#include <linux/cpu.h>

#include "log.h"
#include "x86.h"
#include "nmi.h"
#include "config.h"

static u64 reset_value[MAX_COUNTER];
/*
 * this will be replaced in arch_perfmon_init()
 */
static int num_counters = 2;
static int counter_width = 48;

u64 overflows = 0UL;
u64 samples   = 0UL;

struct counter_config counter_config[MAX_COUNTER];


u64 x86_get_ctrl(struct x86_model_spec *model,
                 struct counter_config *counter_config)
{
        u64 val = 0;
        u16 event = (u16) counter_config->event;

        val |= ARCH_PERFMON_EVENTSEL_INT;

        val |= counter_config->user ? ARCH_PERFMON_EVENTSEL_USR : 0;
        val |= counter_config->kernel ? ARCH_PERFMON_EVENTSEL_OS : 0;
        val |= (counter_config->unit_mask & 0xff) << 8;

        counter_config->extra &= (ARCH_PERFMON_EVENTSEL_INV |
                                  ARCH_PERFMON_EVENTSEL_EDGE |
                                  ARCH_PERFMON_EVENTSEL_CMASK);

        val |= counter_config->extra;

        event &= model->event_mask ? model->event_mask : 0xff;

        val |= event & 0xff;
        val |= (u64)(event & 0x0f00) << 24;

        return val;
}

static inline u64 intel_pmu_get_status(void)
{
        u64 status;

        rdmsrl(MSR_CORE_PERF_GLOBAL_STATUS, status);

        return status;
}

static inline void intel_pmu_ack_status(u64 ack)
{
        wrmsrl(MSR_CORE_PERF_GLOBAL_OVF_CTRL, ack);
}


void intel_pmu_pebs_enable_all(void)
{
        wrmsrl(MSR_IA32_PEBS_ENABLE, 1);
}

void intel_pmu_pebs_disable_all(void)
{
        wrmsrl(MSR_IA32_PEBS_ENABLE, 0);
}

void intel_pmu_reset(void)
{

}

/* NB: this require DS enabled */
void intel_pmu_enable_bts(u64 config)
{
        unsigned long debugctlmsr;

        debugctlmsr = get_debugctlmsr();


        debugctlmsr |= DEBUGCTLMSR_TR;
        debugctlmsr |= DEBUGCTLMSR_BTS;
        debugctlmsr |= DEBUGCTLMSR_BTINT;

        if (!(config & ARCH_PERFMON_EVENTSEL_OS))
                debugctlmsr |= DEBUGCTLMSR_BTS_OFF_OS;

        if (!(config & ARCH_PERFMON_EVENTSEL_USR))
                debugctlmsr |= DEBUGCTLMSR_BTS_OFF_USR;

        update_debugctlmsr(debugctlmsr);
}

void intel_pmu_disable_bts(void)
{
        unsigned long debugctlmsr;

        debugctlmsr = get_debugctlmsr();

        debugctlmsr &=
                ~(DEBUGCTLMSR_TR | DEBUGCTLMSR_BTS | DEBUGCTLMSR_BTINT |
                  DEBUGCTLMSR_BTS_OFF_OS | DEBUGCTLMSR_BTS_OFF_USR);

        update_debugctlmsr(debugctlmsr);
}


/*
 * We only support LBR implementations that have FREEZE_LBRS_ON_PMI
 * otherwise it becomes near impossible to get a reliable stack.
 */
void __intel_pmu_lbr_enable(void)
{
        u64 debugctl;

        rdmsrl(MSR_IA32_DEBUGCTLMSR, debugctl);
        debugctl |= (DEBUGCTLMSR_LBR | DEBUGCTLMSR_FREEZE_LBRS_ON_PMI);
        wrmsrl(MSR_IA32_DEBUGCTLMSR, debugctl);
}


void __intel_pmu_lbr_disable(void)
{
        u64 debugctl;

        rdmsrl(MSR_IA32_DEBUGCTLMSR, debugctl);
        debugctl &= ~(DEBUGCTLMSR_LBR | DEBUGCTLMSR_FREEZE_LBRS_ON_PMI);
        wrmsrl(MSR_IA32_DEBUGCTLMSR, debugctl);
}


static void intel_pmu_disable_all(void)
{

        wrmsrl(MSR_CORE_PERF_GLOBAL_CTRL, 0);
        /*
         * if INTEL_PMC_IDX_FIXED_BTS in cpuc->active_mask
         * intel_pmu_disable_bts()
         */

        /* intel_pmu_pebs_disable_all()
         * intel_pmu_lbr_disable()
         */
}


static void intel_pmu_enable_all(int added)
{
        /* 
         * intel_pmu_pebs_enable_all()
         * intel_pmu_lbr_enable_all()
         */

        wrmsrl(MSR_CORE_PERF_GLOBAL_CTRL, 1);

        /*
         * if INTEL_PMX_IDX_FIXED_BTS in cpuc->active_mask
         * intel_pmu_enable_bts()
         */
}



#ifdef CONFIG_WITH_MUX
static void 
mux_switch_ctrl(struct x86_model_spec *model, struct nmi_msrs *msrs)
{
        u64 val;
        int i;

        LOG("swiching counter\n");

        /* enable active counters */
        for (i = 0; i < num_counters; ++i) {
                int virt = x86_phys_to_virt(i);
                if (!reset_value[virt])
                        continue;

                rdmsrl(msrs->controls[i].addr, val);
                val &= model->reserved;
                val |= x86_get_ctrl(model, &counter_config[virt]);
                wrmsrl(msrs->controls[i].addr, val);
        }
}
#endif


static void intel_start(struct nmi_msrs *msrs)
{
        u64 val;
        int i;

        LOG("enable EVENTSEL_ENABLE\n");
        for (i = 0; i < num_counters; ++i) {
                if (reset_value[i]) {
                        rdmsrl(msrs->controls[i].addr, val);
                        val |= ARCH_PERFMON_EVENTSEL_ENABLE;
                        wrmsrl(msrs->controls[i].addr, val);
                }
        }
}


static void intel_stop(struct nmi_msrs *msrs)
{
        u64 val;
        int i;

        LOG("disabling EVENTSEL_ENABLE\n");
        for (i = 0; i < num_counters; ++i) {
                if (!reset_value[i]) {
                        continue;
                }

                rdmsrl(msrs->controls[i].addr, val);
                val &= ~ARCH_PERFMON_EVENTSEL_ENABLE;
                wrmsrl(msrs->controls[i].addr, val);
        }
}

static void intel_shutdown(struct nmi_msrs *msrs)
{
        int i;

        for (i = 0; i < num_counters; ++i) {
                if (!msrs->counters[i].addr) {
                        continue;
                }

                release_perfctr_nmi(MSR_ARCH_PERFMON_PERFCTR0 + i);
                release_evntsel_nmi(MSR_ARCH_PERFMON_EVENTSEL0 + i);
        }
}

/*
 * this should add data to some ring_buffer type structure
 */
void add_sample(struct pt_regs *regs, unsigned long event)
{
#if 0
        LOGL("event: %lx, adding IP: %lx, SP: %lx, BP: %lx\n",
                event, regs->ip, regs->sp, regs->bp);
#endif
	samples++;
}

/*
 * called from NMI handler,
 * highly sensitive
 */
int intel_check_ctrs(struct pt_regs *regs, struct nmi_msrs *msrs)
{
        u64 val;
        u64 status;
        int i, loops;
        
        apic_write(APIC_LVTPC, APIC_DM_NMI);

        intel_pmu_disable_all();

        status = intel_pmu_get_status();
        if (!status) {
                intel_pmu_enable_all(0);
                return 1;
        }


        loops = 0;
again:
        intel_pmu_ack_status(status);
        if (++loops > 100) {
                static bool warn = false;
                if (!warn) {
                        WARN(1, "irq loop stuck?\n");
                        warn = true;
                }
                intel_pmu_reset();
                goto done;
        }

#if 0
        /*
         * PEBS overflow sets bit 62 in the global status register
         */
        if (__test_and_clear_bit(62, (unsigned long *) &status)) {
                handled++;
                x86_pmu.drain_pebs(regs);
        }
#endif

        for (i = 0; i < num_counters; ++i) {
                if (!reset_value[i]) {
                        continue;
                }

                rdmsrl(msrs->counters[i].addr, val);
                if (val & (1ULL << (counter_width - 1))) {
			overflows++;
                        continue;
                }

                add_sample(regs, i);
		/* this should reset it! */
                wrmsrl(msrs->counters[i].addr, -reset_value[i]);
        }

        status = intel_pmu_get_status();
        if (status)
                goto again;
done:
        intel_pmu_enable_all(0);
        return 1;
}


static void intel_setup_ctrs(struct x86_model_spec *model,
                            struct nmi_msrs *msrs)
{
        u64 val;
        int i;

        LOG("setting up counters\n");

        if (cpu_has_arch_perfmon) {
                union cpuid10_eax eax;
                eax.full = cpuid_eax(0xa);

                /*
                 * For Core2 (family 6, model 15), don't reset the
                 * counter width:
                 */
                if (!(eax.split.version_id == 0 && 
		    __this_cpu_read(cpu_info.x86) == 6 &&
		    __this_cpu_read(cpu_info.x86_model) == 15)) {
                        if (counter_width < eax.split.bit_width)
                                counter_width = eax.split.bit_width;
				LOG("modified counter width to %x\n", counter_width);
                }
        }

        /* clear all counters */
        for (i = 0; i < num_counters; ++i) {
                if (!msrs->controls[i].addr)
                        continue;

                rdmsrl(msrs->controls[i].addr, val);

                if (val & ARCH_PERFMON_EVENTSEL_ENABLE)
                        x86_warn_in_use(i);

                val &= model->reserved;
                wrmsrl(msrs->controls[i].addr, val);

                /*
                 * avoid a false detection of ctr overflows in NMI *
                 * handler
                 */
                wrmsrl(msrs->counters[i].addr, -1LL);
        }

        LOG("enabling counters\n");
        /* enable active counters */
        for (i = 0; i < num_counters; ++i) {

                if (counter_config[i].enabled && msrs->counters[i].addr) {

                        LOG("activating counter %d "
                                "addr: %lx\n", i, msrs->counters[i].addr);

                        reset_value[i] = counter_config[i].count;

			LOG("got counter_config[%d].count %llx\n", 
				i, reset_value[i]);

			LOG("writing to counter: %lx value: %llx\n",
				msrs->counters[i].addr, -reset_value[i]);

                        wrmsrl(msrs->counters[i].addr, -reset_value[i]);

                        rdmsrl(msrs->controls[i].addr, val);
			LOG("control %d at addr: %lx, has value: %llx\n",
				i, msrs->controls[i].addr, val);

                        val &= model->reserved;
                        val |= x86_get_ctrl(model, &counter_config[i]);

			LOG("writing to control addr: %lx, value %llx\n", 
				msrs->controls[i].addr, val);
                        wrmsrl(msrs->controls[i].addr, val);

                } else {
                        reset_value[i] = 0;
                }
        }
}

static int intel_fill_in_addresses(struct nmi_msrs *msrs)
{
        int i;

        LOG("filling in addresses for %d counters\n", num_counters);

        for (i = 0; i < num_counters; i++) {

                if (!reserve_perfctr_nmi(MSR_ARCH_PERFMON_PERFCTR0 + i)) {
                        goto fail;
                }

                if (!reserve_evntsel_nmi(MSR_ARCH_PERFMON_EVENTSEL0 + i)) {
                        release_perfctr_nmi(MSR_IA32_PMC0 + i);
                        goto fail;
                }

                /* both registers must be reserved */
                msrs->counters[i].addr = MSR_ARCH_PERFMON_PERFCTR0 + i;
                LOG("reserved counter %d at addr 0x%lx\n", 
			i, msrs->counters[i].addr);
                msrs->controls[i].addr = MSR_ARCH_PERFMON_EVENTSEL0 + i;
                LOG("reserved control %d at addr 0x%lx\n", 
			i, msrs->controls[i].addr);

                continue;
        fail:
                if (!counter_config[i].enabled)
                        continue;

                x86_warn_reserved(i);
                intel_shutdown(msrs);
                return -EBUSY;
        }

        return 0;
}



static int arch_perfmon_init(struct nmi_operations *ignore)
{
	union cpuid10_eax eax;
	union cpuid10_edx edx;

        eax.full = cpuid_eax(0xa);
	edx.full = cpuid_edx(0xa);

        LOG("arch_perfmon_init(), version %d\n",
		eax.split.version_id);

        /* Workaround for BIOS bugs in 6/15. Taken from perfmon2 */
        if (eax.split.version_id == 0 && 
            __this_cpu_read(cpu_info.x86) == 6 &&
            __this_cpu_read(cpu_info.x86_model) == 15) {

                LOG("fixing BIOS bugs!\n");
                eax.split.version_id = 2;
                eax.split.num_counters = 2;
                eax.split.bit_width = 40;
        } else {
                LOG("BIOS is alright\n");
        }

        num_counters = min((int) eax.split.num_counters, MAX_COUNTER);

        LOG("initialized with %d generalized-counters\n", num_counters);
        arch_perfmon_spec.num_counters = num_counters;
        arch_perfmon_spec.num_controls = num_counters;

	counter_width = eax.split.bit_width;
	LOG("counter width for generalized-counters %d\n",
		counter_width);

	LOG("no of fixed-counters %d, bit_width =%d\n",
		edx.split.num_counters_fixed,
		edx.split.bit_width_fixed);

        return 0;
}

/* should check if proper revision is in place */

static int intel_snb_pebs_broken(int cpu)
{       
        u32 rev = UINT_MAX;
        LOG("CPU=%d\n", cpu);
        switch (cpu_data(cpu).x86_model) {
                case 42: /* SNB */
                        rev = 0x28;
                        break;
                switch (cpu_data(cpu).x86_mask) { /* SNB EP */
                        case 6:
                                rev = 0x618;
                                break;
                        case 7:
                                rev = 0x70c;
                                break;
                }
        }
        LOG("want microcode revision: %x\n", rev);
        LOG("got microcode: %x\n", cpu_data(cpu).microcode);
        return (cpu_data(cpu).microcode < rev);
}

int check_pebs(void)
{
        int i;
        get_online_cpus();
        for_each_online_cpu(i) {
                if (intel_snb_pebs_broken(i))
                        return 1;
        }
        put_online_cpus();
        return 0;
}

struct x86_model_spec arch_perfmon_spec = {

        .reserved               = MSR_PPRO_EVENTSEL_RESERVED,
        .init                   = &arch_perfmon_init,

        /* num_counters/num_controls filled in at runtime */
        .fill_in_addresses      = &intel_fill_in_addresses,

        /* user space does the cpuid check for available events */
        .setup_ctrs             = &intel_setup_ctrs,
        .check_ctrs             = &intel_check_ctrs,
#ifdef CONFIG_WITH_MUX
        .switch_ctrl            = &mux_switch_ctrl,
#endif

        .start                  = &intel_start,
        .stop                   = &intel_stop,
        .shutdown               = &intel_shutdown
};
