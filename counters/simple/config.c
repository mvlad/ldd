#include <linux/kernel.h>

#include "log.h"
#include "config.h"

unsigned int curr_counter_cfg = 0;

inline void add_counter_config(struct counter_config uconfig)
{

        LOG("counter reached %u", curr_counter_cfg);

        counter_config[curr_counter_cfg].count          = uconfig.count;
        counter_config[curr_counter_cfg].enabled        = uconfig.enabled;
        counter_config[curr_counter_cfg].event          = uconfig.event;
        counter_config[curr_counter_cfg].kernel         = uconfig.kernel;
        counter_config[curr_counter_cfg].user           = uconfig.user;
        counter_config[curr_counter_cfg].unit_mask      = uconfig.unit_mask;
        counter_config[curr_counter_cfg].extra          = uconfig.extra;

        curr_counter_cfg++;
}
