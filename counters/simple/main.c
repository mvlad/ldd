#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/version.h>
#include <linux/fs.h>
#include <linux/mm.h>

#include <linux/slab.h>
#include <linux/vmalloc.h>

#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/errno.h>

#include <linux/cpu.h>
#include <linux/smp.h>
#include <linux/idr.h>
#include <linux/proc_fs.h>

#include <linux/stddef.h>
#include <linux/unistd.h>
#include <linux/string.h>
#include <linux/irq_work.h>

#include "log.h"
#include "main.h"
#include "pending.h"
#include "nmi.h"
#include "x86.h"
#include "config.h"

/* ioctl */

static dev_t dev;
static struct cdev c_dev;
static struct class *cl;

DEFINE_PER_CPU(struct nmi_hw_events, nmi_hw_events);
struct nmi_data *data;
struct nmi_operations *nops;

extern u64 nmis, nmis_running, overflows, samples;

/* 
 * how many configs did user-space sents and 
 * we added them.
 *
 * safety to keep track
 */
static int nr_configs = 0;

static struct nmi_data *nmi_alloc(int cpu, void (*overflow_handler) (struct nmi_data *data))
{
	struct nmi_data *data;

	data = kzalloc(sizeof(*data), GFP_KERNEL);

	if (!data)
		return NULL;


	INIT_LIST_HEAD(&data->buff_entry);

	init_waitqueue_head(&data->waitq);
	init_irq_work(&data->pending, pending_data);
	mutex_init(&data->mmap_mutex);

	return data;
}

static struct nmi_operations *init_nmi_ops(void)
{
	struct nmi_operations *nops = kzalloc(sizeof(struct nmi_operations), GFP_KERNEL);

	if (__nmi_init(nops))
                return NULL;

	return nops;
}

static int my_open(struct inode *i, struct file *f)
{
	return 0;
}
static int my_close(struct inode *i, struct file *f)
{
        return 0;
}

int show_ctrs(struct seq_file *seq, void *off)
{
	seq_printf(seq, "NMI: %llx, CTR_RUNNING: %llx, overflows: %llx, samples: %llx\n", 
			nmis, nmis_running, overflows, samples);
	return 0;
}

static int stats_open(struct inode *inode, struct file *file)
{
	return single_open(file, show_ctrs, NULL);
}

static int stats_close(struct inode *inode, struct file *file)
{
	return 0;
}

static long my_ioctl(struct file *f, unsigned int cmd, unsigned long arg)
{
        switch (cmd) {
                case SET_CONFIG: {
                        struct counter_config config;

                        LOG("got info from user-space\n");

                        if (nr_configs > MAX_COUNTER) {
                                LOG("exceeding no of possible configs allowed\n");
                                return -EACCES;
                        }

                        if (model) {
                                LOG("model was initialized\n");
                        }
                        /* 
                         * the model is initializing in __init_nmi 
                         * which in turns initializes num_counters in x86 
                         */
                        if (model && nr_configs > model->num_counters) {
                                LOG("this platform only supports %d counters\n",
                                        model->num_counters);
                                return -EACCES;
                        }

                        if (copy_from_user(&config, (struct counter_config *) arg, 
                                           sizeof(struct counter_config))) {
                                LOG("failed to copy from user entry \n");
                                return -EACCES;
                        }
                        LOG("got from user-space:\n");
                        LOG("count: %lx, enabled: %lx, event: %lx\n",
                                config.count, config.enabled, config.event);
                        LOG("kernel: %lx, user: %lx, unit_mask: %lx, extra: %lx\n",
                                config.kernel, config.user, 
                                config.unit_mask, config.extra);

                        add_counter_config(config);
                        nr_configs++;

                        LOG("at configuration no %d\n", nr_configs);
                        LOG("initializing init_hw\n");
                        nops->setup();
                        break;
                }
                case START_COUNTING : {
                        if (nops) {
                                LOG("starting ...\n");
                                LOG("calling ()\n");
                                nops->start();
                        }
                        break;
                }
                case STOP_COUNTING: {
                        if (nops) {
                                LOG("stopping ...\n");
                                nops->stop();
                        }
                        break;
                }
                default:
                        return -EINVAL;
        }

        return 0;
}

static struct file_operations config_fops =
{
        .owner = THIS_MODULE,
        .open = my_open,
        .release = my_close,
        .unlocked_ioctl = my_ioctl
};
#define FIRST_MINOR     0
#define MINOR_CNT       31

static const struct file_operations proc_stats_ops = {
	.owner          = THIS_MODULE,
	.open           = stats_open,
	.release	= stats_close,
	.read           = seq_read,
	.llseek         = seq_lseek,
	.release        = single_release,
};


static int init_interface(void)
{
        int ret;
        struct device *dret;

        ret = alloc_chrdev_region(&dev, FIRST_MINOR, MINOR_CNT, "qnmi");
        if (ret < 0) {
                return ret;
        }

        cdev_init(&c_dev, &config_fops);

        ret = cdev_add(&c_dev, dev, MINOR_CNT);
        if (ret < 0)
                return ret;

        /* hey, let's create them */
        cl = class_create(THIS_MODULE, "char");
        if (IS_ERR(cl)) {
                cdev_del(&c_dev);
                unregister_chrdev_region(dev, MINOR_CNT);
                return PTR_ERR(cl);
        }

        dret = device_create(cl, NULL, dev, NULL, "inmi");
        if (IS_ERR(dret)) {
                class_destroy(cl);
                cdev_del(&c_dev);
                unregister_chrdev_region(dev, MINOR_CNT);
                return PTR_ERR(dret);
        }

        return 0;
}

static void remove_interface(void)
{
        device_destroy(cl, dev);
        class_destroy(cl);
        cdev_del(&c_dev);
        unregister_chrdev_region(dev, MINOR_CNT);
}

static int __init nmidrv_init(void)
{
        int ret;
	LOG("nmi module init\n");

	
	LOG("allocating data\n");
	data = nmi_alloc(0, NULL);

        LOG("allocating nops\n");
	nops = init_nmi_ops();
        if (!nops)
                goto out_fail;

        LOG("initializing /dev/inmi\n");
        ret = init_interface();
        if (ret) {
                return ret;
        }
	
	LOG("creating stats interface\n");
	proc_create("inmi.stats", S_IWUSR | S_IRUGO, NULL, &proc_stats_ops);

	return 0;

out_fail:
        if (data)
                kfree(data);
        return -EACCES;
}

static void free_data(void)
{
        if (data)
                kfree(data);
        if (nops)
                kfree(nops);
}

/* remove the module */
static void __exit nmidrv_exit(void)
{
	LOG("nmi module exit\n");

        LOG("making sure everything is stopped\n");
        nops->stop();

	LOG("calling shutdown_hw()\n");
	nops->shutdown();

	LOG("freeing data\n");
        free_data();

        LOG("removing /dev/inmi\n");
        remove_interface();

	LOG("removing /proc/inmi.stats\n");
	remove_proc_entry("inmi.stats", NULL);
}

module_init(nmidrv_init);
module_exit(nmidrv_exit);

MODULE_AUTHOR("Marius Vlad<mv@sec.uni-passau.de>");
MODULE_DESCRIPTION("NMI");
MODULE_LICENSE("GPL");
