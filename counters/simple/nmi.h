#ifndef __NMI_H
#define __NMI_H

struct super_block;
struct dentry;
struct file_operations;
struct pt_regs;

struct nmi_operations {
        /* create any necessary configuration files in the oprofile fs.
         * Optional. */
        int (*create_files)(struct super_block * sb, struct dentry * root);
        /* Do any necessary interrupt setup. Optional. */
        int (*setup)(void);
        /* Do any necessary interrupt shutdown. Optional. */
        void (*shutdown)(void);
        /* Start delivering interrupts. */
        int (*start)(void);
        /* Stop delivering interrupts. */
        void (*stop)(void);

        /* Arch-specific buffer sync functions.
         * Return value = 0:  Success
         * Return value = -1: Failure
         * Return value = 1:  Run generic sync function
         */
        int (*sync_start)(void);
        int (*sync_stop)(void);

        /* Initiate a stack backtrace. Optional. */
        void (*backtrace)(struct pt_regs * const regs, unsigned int depth);

        /* Multiplex between different events. Optional. */
        int (*switch_events)(void);
        /* CPU identification string. */
        char * cpu_type;
};

extern int __nmi_init(struct nmi_operations *ops);
extern struct x86_model_spec *model;

inline int x86_phys_to_virt(int phys);
inline int x86_virt_to_phys(int virt);

#endif /* __NMI_H */
