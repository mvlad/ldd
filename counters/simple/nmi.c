#include <linux/cpu.h>
#include <linux/smp.h>
#include <linux/idr.h>
#include <linux/percpu.h>
#include <asm/apic.h>
#include <asm/stacktrace.h>
#include <asm/nmi.h>
#include <linux/slab.h>

#include "log.h"
#include "nmi.h"
#include "pending.h"
#include "x86.h"
#include "config.h"

/* must be protected with 
   get_online_cpus()/put_online_cpus(): */
static int nmi_enabled;
static int ctr_running;
struct x86_model_spec *model;

static DEFINE_PER_CPU(unsigned long, saved_lvtpc);
static DEFINE_PER_CPU(struct nmi_msrs, cpu_msrs);
static DEFINE_RAW_SPINLOCK(nmi_lock);

#ifdef CONFIG_WITH_MUX
static DEFINE_PER_CPU(int, switch_index);
#endif

extern struct counter_config counter_config[];

static void nmi_cpu_start(void *dummy);
static void nmi_cpu_stop(void *dummy);

u64 nmis = 0UL;
u64 nmis_running = 0UL;

/*
 * this is the NMI handler
 */
static int exceptions_notify(unsigned int val, struct pt_regs *regs)
{
        if (ctr_running) {
                model->check_ctrs(regs, &__get_cpu_var(cpu_msrs));
		nmis_running++;
        } else if (!nmi_enabled) {
                return NMI_DONE;
        } else {
                model->stop(&__get_cpu_var(cpu_msrs));
        }

	nmis++;
        return NMI_HANDLED;
}

#ifdef CONFIG_WITH_MUX

static inline int has_mux(void)
{
        return !!model->switch_ctrl;
}

inline int x86_phys_to_virt(int phys)
{
        return __this_cpu_read(switch_index) + phys;
}

inline int x86_virt_to_phys(int virt)
{
        return virt % model->num_counters;
}

static void nmi_shutdown_mux(void)
{
        int i;

        if (!has_mux())
                return;

        for_each_possible_cpu(i) {
                kfree(per_cpu(cpu_msrs, i).multiplex);
                per_cpu(cpu_msrs, i).multiplex = NULL;
                per_cpu(switch_index, i) = 0;
        }
}

static int nmi_setup_mux(void)
{
        size_t multiplex_size =
                sizeof(struct nmi_msr) * model->num_virt_counters;
        int i;

        if (!has_mux())
                return 1;

        for_each_possible_cpu(i) {
                per_cpu(cpu_msrs, i).multiplex = kzalloc(multiplex_size, GFP_KERNEL);

                if (!per_cpu(cpu_msrs, i).multiplex)
                        return 0;
        }

        return 1;
}

static void nmi_cpu_setup_mux(int cpu, struct nmi_msrs *msrs)
{
        int i;
        struct nmi_msr *multiplex = msrs->multiplex;

        if (!has_mux())
                return;

        LOG("setting up mux\n");
        for (i = 0; i < model->num_virt_counters; ++i) {
                if (counter_config[i].enabled) {
                        multiplex[i].saved = -(u64) counter_config[i].count;
                } else {
                        multiplex[i].saved = 0;
                }
        }

        per_cpu(switch_index, cpu) = 0;
}



static void nmi_cpu_save_mpx_registers(struct nmi_msrs *msrs)
{
        struct nmi_msr *counters = msrs->counters;
        struct nmi_msr *multiplex = msrs->multiplex;
        int i;

        for (i = 0; i < model->num_counters; ++i) {
                int virt = x86_phys_to_virt(i);
                if (counters[i].addr)
                        rdmsrl(counters[i].addr, multiplex[virt].saved);
        }
}

static void nmi_cpu_restore_mpx_registers(struct nmi_msrs *msrs)
{
        struct nmi_msr *counters = msrs->counters;
        struct nmi_msr *multiplex = msrs->multiplex;
        int i;

        for (i = 0; i < model->num_counters; ++i) {
                int virt = x86_phys_to_virt(i);
                if (counters[i].addr)
                        wrmsrl(counters[i].addr, multiplex[virt].saved);
        }
}


static void nmi_cpu_switch(void *dummy)
{
        int cpu = smp_processor_id();
        int si  = per_cpu(switch_index, cpu);

        struct nmi_msrs *msrs = &per_cpu(cpu_msrs, cpu);

        LOG("nmi_cpu_switch invoked\n");

        nmi_cpu_stop(NULL);
        nmi_cpu_save_mpx_registers(msrs);

        si += model->num_counters;
        if ((si >= model->num_virt_counters) || 
            (counter_config[si].count == 0))
                per_cpu(switch_index, cpu) = 0;
        else
                per_cpu(switch_index, cpu) = si;

        model->switch_ctrl(model, msrs);
        nmi_cpu_restore_mpx_registers(msrs);

        nmi_cpu_start(NULL);
}

static int nmi_multiplex_on(void)
{
        return counter_config[model->num_counters].count ? 0 : -EINVAL;
}

static int nmi_switch_event(void)
{
        if (!has_mux())
                return -ENOSYS;
        if (nmi_multiplex_on() < 0)
                return -EINVAL;

        LOG("got switch event\n");

        get_online_cpus();
        if (ctr_running) {
                LOG("should switch!\n");
                on_each_cpu(nmi_cpu_switch, NULL, 1);
        } else {
                LOG("ctr not currently running\n");
        }
        put_online_cpus();

        return 0;
}

static inline void mux_init(struct nmi_operations *ops)
{
        LOG("invoked mux_init\n");
        if (has_mux()) {
                LOG("detected mux!\n");
                ops->switch_events = nmi_switch_event;
        } else {
                LOG("not detecting mux!\n");
        }

}
static void mux_clone(int cpu)
{
        if (!has_mux())
                return;

        memcpy(per_cpu(cpu_msrs, cpu).multiplex,
               per_cpu(cpu_msrs, 0).multiplex,
               sizeof(struct nmi_msr) * model->num_virt_counters);
}
#else

inline int x86_virt_to_phys(int virt) { return virt; }
inline int x86_phys_to_virt(int phys) { return phys; }

static int nmi_setup_mux(void) { return 1; }
static void nmi_shutdown_mux(void) { }

static void nmi_cpu_setup_mux(int cpu, struct nmi_msrs *msrs) { }

static inline void mux_init(struct nmi_operations *ops) { }
static void mux_clone(int cpu) { }
#endif

static void nmi_cpu_save_registers(struct nmi_msrs *msrs)
{
        struct nmi_msr *counters = msrs->counters;
        struct nmi_msr *controls = msrs->controls;
        unsigned int i;

        LOG("saving registers\n");

        for (i = 0; i < model->num_counters; ++i) {
                if (counters[i].addr)
                        rdmsrl(counters[i].addr, counters[i].saved);
        }

        for (i = 0; i < model->num_controls; ++i) {
                if (controls[i].addr)
                        rdmsrl(controls[i].addr, controls[i].saved);
        }
}

static void nmi_cpu_restore_registers(struct nmi_msrs *msrs)
{
        struct nmi_msr *counters = msrs->counters;
        struct nmi_msr *controls = msrs->controls;
        unsigned int i;

        for (i = 0; i < model->num_controls; ++i) {
                if (controls[i].addr)
                        wrmsrl(controls[i].addr, controls[i].saved);
        }

        for (i = 0; i < model->num_counters; ++i) {
                if (counters[i].addr)
                        wrmsrl(counters[i].addr, counters[i].saved);
        }
}

static void nmi_cpu_setup(void *dummy)
{
        int cpu = smp_processor_id();

	struct nmi_msrs *msrs = &per_cpu(cpu_msrs, cpu);

        LOG("setting up CPU=%d\n", cpu);

	nmi_cpu_save_registers(msrs);

	raw_spin_lock(&nmi_lock);

	model->setup_ctrs(model, msrs);
	nmi_cpu_setup_mux(cpu, msrs);

	raw_spin_unlock(&nmi_lock);

        LOG("saving APIC_LVTPC\n");
        per_cpu(saved_lvtpc, cpu) = apic_read(APIC_LVTPC);

        LOG("writing APIC_DM_NMI\n");

        apic_write(APIC_LVTPC, APIC_DM_NMI);
}

static void nmi_cpu_shutdown(void *dummy)
{
        unsigned int v;
        int cpu = smp_processor_id();
	struct nmi_msrs *msrs = &per_cpu(cpu_msrs, cpu);

        /* restoring APIC_LVTPC can trigger an apic error because the delivery
         * mode and vector nr combination can be illegal. That's by design: on
         * power on apic lvt contain a zero vector nr which are legal only for
         * NMI delivery mode. So inhibit apic err before restoring lvtpc
         */

        v = apic_read(APIC_LVTERR);
        apic_write(APIC_LVTERR, v | APIC_LVT_MASKED);
        apic_write(APIC_LVTPC, per_cpu(saved_lvtpc, cpu));
        apic_write(APIC_LVTERR, v);

	nmi_cpu_restore_registers(msrs);
}


static void nmi_cpu_start(void *dummy)
{
	struct nmi_msrs *msrs = &__get_cpu_var(cpu_msrs);

	LOG("starting nmi cpu\n");
	model->start(msrs);
}

static void nmi_cpu_stop(void *dummy)
{
	struct nmi_msrs *msrs = &__get_cpu_var(cpu_msrs);

	LOG("stopping nmi cpu\n");
	model->stop(msrs);
}


static void nmi_cpu_up(void *dummy)
{
	LOG("nmi_cpu_up()\n");

        if (nmi_enabled)
                nmi_cpu_setup(dummy);
        if (ctr_running)
                nmi_cpu_start(dummy);
}

static void nmi_cpu_down(void *dummy)
{
	LOG("nmi_cpu_down()\n");

        if (ctr_running)
                nmi_cpu_stop(dummy);
        if (nmi_enabled)
                nmi_cpu_shutdown(dummy);
}


static int nmi_cpu_notifier(struct notifier_block *b, 
			    unsigned long action,
                            void *data)
{
        int cpu = (unsigned long) data;

        LOG("calling NMI CPU notifier\n");

        switch (action) {
        	case CPU_DOWN_FAILED:
                        LOG("CPU_DOWN_FAILED, fallthru\n");
        	case CPU_ONLINE:
                        LOG("CPU_ONLINE\n");
                	smp_call_function_single(cpu, nmi_cpu_up, NULL, 0);
                break;
        	case CPU_DOWN_PREPARE:
                        LOG("CPU_DOWN_PREPARE\n");
                	smp_call_function_single(cpu, nmi_cpu_down, NULL, 1);
                break;
        }

        return NOTIFY_DONE;
}

static struct notifier_block nmi_cpu_nb = {
        .notifier_call = nmi_cpu_notifier
};

static void free_msrs(void)
{
        int i;
        LOG("freeing up msrs\n");

        for_each_possible_cpu(i) {

                kfree(per_cpu(cpu_msrs, i).counters);
                per_cpu(cpu_msrs, i).counters = NULL;

                kfree(per_cpu(cpu_msrs, i).controls);
                per_cpu(cpu_msrs, i).controls = NULL;
        }

        nmi_shutdown_mux();
}

static int allocate_msrs(void)
{
        size_t controls_size = sizeof(struct nmi_msr) * model->num_controls;
        size_t counters_size = sizeof(struct nmi_msr) * model->num_counters;
        int i;

        LOG("allocating msrs\n");
        for_each_possible_cpu(i) {

                per_cpu(cpu_msrs, i).counters = kzalloc(counters_size, GFP_KERNEL);

                if (!per_cpu(cpu_msrs, i).counters) {
                        goto fail;
		}

                per_cpu(cpu_msrs, i).controls = kzalloc(controls_size, GFP_KERNEL);

                if (!per_cpu(cpu_msrs, i).controls) {
                        goto fail;
		}

        }

        if (!nmi_setup_mux()) {
                LOG("failed to setup mux\n");
                goto fail;
        }

        return 1;
fail:
        free_msrs();
        return 0;
}


static int init_hw(void)
{
	int err = 0;
	int cpu;

	if (!allocate_msrs())
		return -ENOMEM;

	err = model->fill_in_addresses(&per_cpu(cpu_msrs, 0));
	if (err) {
		goto fail;
        }

	for_each_possible_cpu(cpu) {
		if (!cpu) {
			continue;
		}
		memcpy(per_cpu(cpu_msrs, cpu).counters,
		       per_cpu(cpu_msrs, 0).counters,
		       sizeof(struct nmi_msr) * model->num_counters);

		memcpy(per_cpu(cpu_msrs, cpu).controls,
		       per_cpu(cpu_msrs, 0).controls,
		       sizeof(struct nmi_msr) * model->num_controls);

		mux_clone(cpu);
	}

	nmi_enabled = 0;
	ctr_running = 0;
	/* make variables visible to the nmi handler: */
	smp_mb();

	LOG("registering NMI handler\n");
	err = register_nmi_handler(NMI_LOCAL, exceptions_notify, 0, "BNMI");
	if (err) {
                LOG("could not register nmi handler\n");
		return 1;
        }

	get_online_cpus();

	LOG("registering cpu notifier\n");
	register_cpu_notifier(&nmi_cpu_nb);
	nmi_enabled = 1;

	/* make nmi_enabled visible to the nmi handler */
	smp_mb();

	LOG("do invoke nmi_cpu_setup\n");

	on_each_cpu(nmi_cpu_setup, NULL, 1);
	put_online_cpus();

	return 0;

fail:
        free_msrs();
        return err;
}

static void shutdown_hw(void)
{
	struct nmi_msrs *msrs;

	get_online_cpus();
	LOG("unregistering cpu notifier\n");
	unregister_cpu_notifier(&nmi_cpu_nb);

	LOG("do invoke nmi_cpu_shutdown\n");
	on_each_cpu(nmi_cpu_shutdown, NULL, 1);

	nmi_enabled = 0;
	ctr_running = 0;
	put_online_cpus();
	/* make variables visible to the nmi handler: */
	smp_mb();

	LOG("unregistering NMI handler\n");
	unregister_nmi_handler(NMI_LOCAL, "BNMI");

	msrs = &get_cpu_var(cpu_msrs);

	model->shutdown(msrs);
	free_msrs();

	put_cpu_var(cpu_msrs);
}

static int nmi_start(void)
{

	LOG("nmi_start()\n");

        get_online_cpus();
        ctr_running = 1;
        /* make ctr_running visible to the nmi handler: */
        smp_mb();
        on_each_cpu(nmi_cpu_start, NULL, 1);
        put_online_cpus();

        return 0;
}

static void nmi_stop(void)
{
	LOG("nmi_stop()\n");

        get_online_cpus();
        on_each_cpu(nmi_cpu_stop, NULL, 1);
        ctr_running = 0;
        put_online_cpus();
}

int __nmi_init(struct nmi_operations *ops)
{
	__u8 vendor = boot_cpu_data.x86_vendor;
	__u8 family = boot_cpu_data.x86;
        __u8 micro_model  = boot_cpu_data.x86_model;

	char *cpu_type = NULL;
	int ret = 0;

	switch (vendor) {
		case X86_VENDOR_AMD: {
			LOG("got AMD vendor\n");
			return -ENODEV;
			break;
		}
		case X86_VENDOR_INTEL: {
			switch (family) {
				case 0xf:
					LOG("P4 vendor\n");
					break;
				case 0x6:
					LOG("PPRO vendor\n");
					break;
				default:
					LOG("Intel vendor...\n");
					break;
			}
                        LOG("model: ");
                        switch (micro_model) {
                                case 42:
                                case 45:
                                        /* Sandy */
                                        LOG("SandyBridge\n");
                                        if (check_pebs()) {
                                                LOG("Please update microcode.");
                                                return 1;
                                        }
                                        break;
                                case 58:
                                case 62:
                                        /* Ivy */
                                        LOG("IvyBridge\n");
                                        break;
                                case 60:
                                case 70:
                                case 71:
                                case 63:
                                        /* Haswell */
                                        LOG("Haswell\n");
                                        break;
                                default:
                                        LOG("Intel unknown\n");
                                        break;
                        }
                        LOG("Assigned arch perfmon\n");
			cpu_type = "arch_perform";
			model = &arch_perfmon_spec;
			break;
		}
		default:
			return -ENODEV;
			break;
	}

        LOG("assigning cbs\n");

	ops->setup              = init_hw;
	ops->shutdown           = shutdown_hw;

	ops->start              = nmi_start;
	ops->stop               = nmi_stop;

	ops->cpu_type           = cpu_type;

	if (model->init) {
                LOG("model has init. executing\n");
		ret = model->init(ops);
        }

        if (ret) {
                return ret;
        }

        if (!model->num_virt_counters)
                model->num_virt_counters = model->num_counters;

        mux_init(ops);

	LOG("inited nmi\n");

	return ret;
}
