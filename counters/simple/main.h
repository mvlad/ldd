#ifndef __MAIN_H
#define __MAIN_H

#define MAX_NMI_EVENTS  		64

#define SET_CONFIG              _IOR('n', 0xf0, struct counter_config *)
#define START_COUNTING          _IO('n', 0xf1)
#define STOP_COUNTING           _IO('n', 0xf2)

struct buffer {
        /* POLL_ for wakeups */
        atomic_t                poll;
        /* more poll req */
        spinlock_t              nmi_lock;
        struct list_head        nmi_list;

        /* wake up water mark */
        long                    watermark;

        struct work_struct      work;
};

struct nmi_data {
	struct list_head	nmi_entry;
	atomic_long_t		refcount;

        atomic_t                nmi_limit;

        struct mutex            mmap_mutex;
	atomic_t		mmap_count;
	int			cpu;

        struct buffer           *buff;
        struct list_head        buff_entry;

        wait_queue_head_t       waitq;
        struct fasync_struct    *fasync;

        /* delayed work */
        int                     pending_wakeup;
        int                     pending_kill;
        int                     pending_disable;
        struct irq_work         pending;

	struct rcu_head         rcu_head;


        /* overflow handler */
        void                    (*overflow_handler)(struct nmi_data *data);
};

struct nmi_hw_events {
	struct nmi_data *data[MAX_NMI_EVENTS];
	int		enabled;
	u64		ctrl_virt_mask;

};

#endif /* __NMI_H */
