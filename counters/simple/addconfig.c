#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <time.h>
#include <sys/time.h>
#include <stddef.h>

#include "config.h"

#define SET_CONFIG              _IOR('n', 0xf0, struct counter_config *)
#define START_COUNTING          _IO('n', 0xf1)
#define STOP_COUNTING           _IO('n', 0xf2)

#define dprintf(fmt, args...)   do { fprintf(stderr, fmt, ##args); } while (0)

const char dev_device[] = "/dev/inmi";

#define CONFIG_CTR      1 << 1
#define START_CTR       1 << 2
#define STOP_CTR        1 << 3

#define FLAG_MASK                       (CONFIG_CTR | START_CTR | STOP_CTR)
#define IS_FLAG_SET(flag, x)            ((flag & FLAG_MASK) == x)

#define num_to_mask(x)  ((1U << (x)) - 1)

typedef struct {
        unsigned int eax, ebx, ecx, edx;
} cpuid_data;

static inline void cpuid(int func, cpuid_data *cdata)
{
        asm("cpuid"
                : "=a" (cdata->eax), "=b" (cdata->ebx), "=c" (cdata->ecx), "=d" (cdata->edx)
                : "0" (func));
}

static inline unsigned get_ctr_mask(void)
{
        cpuid_data data;
        cpuid(0xa, &data);
        return num_to_mask((data.eax >> 8) & 0xff);
}

int main(int argc, char *argv[])
{
        int ret;
        int fd;
        struct counter_config cfg;
        struct timespec ts;
        int flag, c;

        if (argc < 2) {
                dprintf("Usage: %s [-s|-c|-t]\n", argv[0]);
                exit(EXIT_FAILURE);
        }

        flag = 0x0;
        while ((c = getopt(argc, argv, "cst")) != -1) {
                switch (c) {
                        case 'c':
                                flag = CONFIG_CTR;
                                break;
                        case 's':
                                flag |= START_CTR;
                                break;
                        case 't':
                                flag = STOP_CTR;
                                break;
                        default:
                                dprintf("invalid flag given\n");
                                exit(EXIT_FAILURE);
                                break;
                }
        }

        if (access(dev_device, R_OK | W_OK) == -1) {
                dprintf("failed to read/write to device\n");
                exit(EXIT_FAILURE);
        }
        
        if ((fd = open(dev_device, O_RDWR)) == -1) {
                dprintf("could not open device\n");
                exit(EXIT_FAILURE);
        }

	/*
	 * event:0xc4 counters:0,1 
	 * um:zero minimum:500 
	 * name:BR_INST_RETIRED : number of branch instructions retired
	 */
        cfg.enabled = 1;
        cfg.event = 0xc4;
	/*
	 * generalized-counters start from 0xc1 .. 0xcN
	 * see MSR_ARCH_PERFMON_PERFCTR0
	 * 
	 * each counter is associated with a EVTSEL counter, 
	 * and starts from 0x186
	 * see MSR_ARCH_PERFMON_EVENTSEL0
	 */
        cfg.count = 0xc1;
        cfg.kernel = 1;
        cfg.user = 0;
        cfg.unit_mask = 0x00;
        cfg.extra = 0x00;

        if (IS_FLAG_SET(flag, CONFIG_CTR)) {
                dprintf("seding data to user-space\n");
                dprintf("enabled: %lx, event: %lx, count: %lx\n"
                        "kernel: %lx, user: %lx, unit_mask: %lx "
                        "extra: %lx\n", cfg.enabled, cfg.event,
                        cfg.count, cfg.kernel, cfg.user,
                        cfg.unit_mask, cfg.extra);
                ret = ioctl(fd, SET_CONFIG, &cfg);

                if (ret < 0) {
                        dprintf("could not set data\n");
			close(fd);
                        exit(EXIT_FAILURE);
                }
        }
        if ((flag & FLAG_MASK) == START_CTR) {
                dprintf("activating counters\n");
                ret = ioctl(fd, START_COUNTING);
        } else if ((flag & FLAG_MASK) == STOP_CTR) {
                dprintf("stopping counters\n");
                ret = ioctl(fd, STOP_COUNTING);
        }

        if (ret < 0) {
                dprintf("could not start|stop counting\n");
		
        }

        close(fd);
        return 0;
}
