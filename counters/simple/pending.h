#ifndef __PENDING_H
#define __PENDING_H

#include <linux/irq_work.h>

extern int event_nmi_handler(unsigned int cmd, struct pt_regs *regs);
extern void pending_data(struct irq_work *work);

#endif /* __PENDING_H */
