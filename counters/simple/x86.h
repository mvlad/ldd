#ifndef __X86_H
#define __X86_H

#include <asm/types.h>
#include <asm/perf_event.h>
#include <linux/seq_file.h>

#define MAX_COUNTER			32
#define MSR_PPRO_EVENTSEL_RESERVED      ((0xFFFFFFFFULL << 32) | (1ULL << 21))

struct nmi_msr {
        unsigned long   addr;
        u64             saved;
};

struct nmi_msrs {
        struct nmi_msr *counters;
        struct nmi_msr *controls;
        struct nmi_msr *multiplex;
};

struct nmi_operations;

struct x86_model_spec {
	unsigned int    num_counters;
        unsigned int    num_controls;
        unsigned int    num_virt_counters;
	u64             reserved;
	u16             event_mask;

        int             (*init)(struct nmi_operations *ops);
        void            (*shutdown)(struct nmi_msrs *msrs);

        void            (*start)(struct nmi_msrs *msrs);
        void            (*stop)(struct nmi_msrs *msrs);

	void            (*setup_ctrs)(struct x86_model_spec *model, struct nmi_msrs *msrs);
	int             (*check_ctrs)(struct pt_regs *regs, struct nmi_msrs *msrs);

	int             (*fill_in_addresses)(struct nmi_msrs *msrs);
#ifdef CONFIG_WITH_MUX
        void            (*switch_ctrl)(struct x86_model_spec *model, struct nmi_msrs *msrs);
#endif
};


static inline void x86_warn_in_use(int counter)
{
        /*
         * The warning indicates an already running counter. If
         * oprofile doesn't collect data, then try using a different
         * performance counter on your platform to monitor the desired
         * event. Delete counter #%d from the desired event by editing
         * the /usr/share/oprofile/%s/<cpu>/events file. If the event
         * cannot be monitored by any other counter, contact your
         * hardware or BIOS vendor.
         */
        pr_warning("counter #%d on cpu #%d may already be used\n", 
		counter, smp_processor_id());
}

static inline void x86_warn_reserved(int counter)
{
        pr_warning("counter #%d is already reserved\n", counter);
}

extern struct x86_model_spec arch_perfmon_spec;
extern int check_pebs(void);

#endif /* __X86_H */
