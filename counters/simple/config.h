#ifndef __CONFIG_H
#define __CONFIG_H

struct counter_config {
        unsigned long count;
        unsigned long enabled;
        unsigned long event;
        unsigned long kernel;
        unsigned long user;
        unsigned long unit_mask;
        unsigned long extra;
};

extern struct counter_config counter_config[];
extern unsigned int curr_counter_cfg;
inline void add_counter_config(struct counter_config uconfig);

#endif /* __CONFIG_H */
